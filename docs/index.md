### WaykiChain Developer Portal 

Welcome to the WaykiChain Developer Documentation v3.0.

WaykiChain developer documentation v3.0 corresponds to the current WaykiChain public chain 3.0 (WaykiChain has been upgraded to v3.0 as of Jul 2020)

 WaykiChain is a free, open-source blockchain software solution that provides independent developers to commercial enterprises a platform to build, deploy and run supercharged blockchain applications

<br>

---

### WaykiChain 

WaykiChain focuses on blockchain technology development and related project operations, with industry-leading third-generation blockchain commercial public chain, high-performance transaction processing capabilities, efficient consensus mechanism, powerful intelligent contract engine, and blockchain upgrade management capabilities. It can provide blockchain basic service facilities and industry-level solutions for major industries and important vertical areas.

The self-developed public-chain wiki chain is a smart contract platform that supports Turing. It adopts the internationally advanced DPoS consensus mechanism and has 11 voting nodes. The transaction speed has been confirmed to be stable at 3200 pens/second.

At present, the WaykiChain public chain has integrated a stable currency decentralization system, which stabilizes the currency WUSDagainst the US dollar. The governance currency WGRTis the core token of the stable currency decentralized system, and is WICC still the main chain of the public chain. Multi-currency transfers are supported on the chain, and contracts support multi-currency calls.

<br>

---

### Github Source Code
[https://github.com/WaykiChain](https://github.com/WaykiChain)

<br>

---


### Consensus mechanism 
The consensus algorithm used is `DPOS+PBFT`

### Total number of Nodes
There are a total of 11 super nodes

### Block speed
One block every 3 seconds

<br>

---
