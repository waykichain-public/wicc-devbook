# upgrade guide
**`$WaykichainDockerName` means name of container**  
Take the main network for example:

## 1. stop container

1.operator in your container  

> `coind stop`

2.operator in your IDE   

> `docker exec -it $WaykichainDockerName coind stop`  

<br>

## 2. remove container

> `docker rm $WaykichainDockerName`

<br>

## 3. delete local data(you can ignored this step, if there is no large change in version.)
**For example**  

you can ignored where version from v3.0.0 to v3.0.1  
you should performed this step where version from v2.0 to v3.0
 
### 1.finding the localtion of local data
finding startup script， `run-waykicoind-main.sh`

> `cat run-waykicoind-main.sh`

```
docker run --name waykicoind-mainnet -p 8920:8920 -p 6968:6968 \
  -v `pwd`/conf/WaykiChain.conf:/root/.WaykiChain/WaykiChain.conf \
  -v `pwd`/data:/root/.WaykiChain/main \
  -v `pwd`/bin:/opt/wicc \
  -v `pwd`/lua:/tmp/lua \
  -v `pwd`/wasm:/tmp/wasm \
  -d wicc/waykicoind
```

as shown, the localtion of data is `pwd`/data

> `ll data`

```
-rw-------   1 hpy  staff     0B  7 13 09:32 ERROR.log
-rw-------   1 hpy  staff     0B  7 13 09:32 INFO.log
drwx------  20 hpy  staff   640B  7 20 15:57 blocks
drwx------   3 hpy  staff    96B  7 20 16:50 database
-rw-------   1 hpy  staff     0B  7 13 09:32 db.log
-rw-------   1 hpy  staff     0B  7 20 15:57 dberr.log
-rw-------   1 hpy  staff     0B  7 13 09:32 debug.log
-rw-------   1 hpy  staff   1.1K  7 20 18:20 peers.dat
-rw-------   1 hpy  staff    16K  7 20 16:50 wallet.dat
-rw-------   1 hpy  staff    36K  7 13 14:02 wallet.dat.bak
```

`blocks` : need delete directory file.

### 2.delete data

> `rm -rf data/blocks`

### 3.if your had download WaykiChain Binary Releases,you should decompressing files, and Place in the data file

path: pwd/data/`blocks`

<br>

## 4. pull from DockerHub

show the existinf images

> `docker images`

```
REPOSITORY               TAG                 IMAGE ID            CREATED             SIZE
wicc/waykicoind          3.2                42dc5f320fdb        5 hours ago         966MB
```

delete old images
> `docker rmi $oldImagesName`

pull the new images
> `docker pull wicc/waykicoind:3.2`

<br>

## 5. start container  
Enter the directory `WaykiChain_mainnet`.
> `sh bin/run-waykicoind-main.sh`

<br>

## 6. Check the result of the upgrade
> `coind getinfo`

```
"version" : "v3.2.0.1-78dc7178-release-linux (2020-08-04 18:52:02 +0800)",
```

if the data is 07-17, it show successful. if appear
`error: couldn't connect to server... pls wait for a while or check "rpcserver=1" setting.`  
it means local data is Loading, in the software startup, please wait patiently   
 
**attention：it's not allowed to interact until 6 minutes after main network is up**
