### Running WaykiChain in Docker

Here we will go through how to run WaykiChain coind inside a Docker container! If you dont know what docker is then you should run over to [docker.io](docker.io) and learn about it asap as it's a must have tool. 

---

<br>

## Install Dependencies
  * Docker 17.05 or higher is required
  
## Docker Environment Requirement
  * MacOS, Ubuntu 14,16,18 or CentOS 7.
  * At least 8GB RAM (Docker -> Preferences -> Advanced -> Memory -> 8GB or above)
  * If the build below fails, make sure you've adjusted Docker Memory settings and try again.

---

<br>

## Build WaykiCoind Docker image

<br>

##### Method 1:

**Pull from DockerHub**

``` 
docker pull wicc/waykicoind:3.2    
```

##### Method 2:
**Build from Dockerfile**
 
1. ```git clone https://github.com/WaykiChain/WaykiChain.git```
2. ```cd WaykiChain/docker && sh ./bin/build-waykicoind.sh```


<br>

**Check Image**  
```
docker images
```

**Result**
```
REPOSITORY               TAG                 IMAGE ID            CREATED             SIZE
wicc/waykicoind          3.2                035aa25cfa9a        7 days ago          966MB  
```
---

<br>
<br>

---
