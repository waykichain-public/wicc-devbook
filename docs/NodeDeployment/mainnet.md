### Setting up Docker and Linux (Ubuntu / CentOS) 
We go through how to use Docker and Linux to run a node on Mainnet. Lets start with Docker

<br>

---
### Docker
---

<br>


 Refer to [build for docker][1] And [WaykiChain.conf][2]

### The local directory `/opt/wicc` is structured as follows
```
~/workspace/wicc/WaykiChain_mainnet$tree
.
├── bin
│   └── run-waykicoind-main.sh
└── conf
    └── WaykiChain.conf

```

#### Startup script content `run-waykicoind-main.sh`
```shell
docker run --name waykicoind-mainnet -p 8920:8920 -p 6968:6968 \
  -v `pwd`/conf/WaykiChain.conf:/root/.WaykiChain/WaykiChain.conf \
  -v `pwd`/data:/root/.WaykiChain/main \
  -v `pwd`/bin:/opt/wicc/bin \
  -v `pwd`/lua:/tmp/lua \
  -v `pwd`/wasm:/tmp/wasm \
  -d wicc/waykicoind:3.2
```

#### Configuration file content `WaykiChain.conf`
```
#nettype=main|test|regtest
nettype=main
rpcserver=1
rpcallowip=0.0.0.0/0
rpcport=6968
rpcuser=wiccuser
rpcpassword=wicc1000
rpcthreads=8
logprinttoconsole=0
logprinttofile=1
logprintfileline=1
logtimestamps=1
listen=1
disablesafemode=1
genblock=0
genblocklimit=1000000
debug=INFO
```

**Note:** 
genblock is for delegates and not necessary to have in the conf. It defaults to false if not included in the conf.

### Start node

`sh bin/run-waykicoind-main.sh`

**example**

```
\~/workspace/wicc/WaykiChain_mainnet$sh bin/run-waykicoind-main.sh
6594c134fb1fafa5804a86c8b4c03603830339029439ea2d4d41724e4716c502
\~/workspace/wicc/WaykiChain_mainnet$docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                                                                 NAMES
6594c134fb1f        wicc/waykicoind     "./coind"           5 seconds ago       Up 4 seconds        0.0.0.0:6968-\>6968/tcp, 0.0.0.0:8920-\>8920/tcp             waykicoind-mainnet
\~/workspace/wicc/WaykiChain_mainnet$
```

### Check if the node is working properly

`docker exec -it waykicoind-mainnet /bin/bash`  

**example**

```
\~/workspace/wicc/WaykiChain_mainnet$docker exec -it waykicoind-mainnet /bin/bash
root@6594c134fb1f:/opt/wicc# coind getinfo
{
    "version" : "v3.2.0.1-78dc7178-release-linux (2020-08-04 18:52:02 +0800)",
    "protocol_version" : 10001,
    "net_type" : "MAIN_NET",
    "proxy" : "",
    "public_ip" : "116.25.40.216",
    "conf_dir" : "/root/.WaykiChain/WaykiChain.conf",
    "data_dir" : "/root/.WaykiChain/main",
    "block_interval" : 10,
    "genblock" : 0,
    "time_offset" : 0,
    "WICC_balance" : 0,
    "WUSD_balance" : 0,
    "WGRT_balance" : 0,
    "relay_fee_perkb" : 1000,
    "tipblock_tx_count" : 2,
    "tipblock_fuel_rate" : 1,
    "tipblock_fuel" : 10,
    "tipblock_time" : 1528964950,
    "tipblock_hash" : "80c0d33440cb92168f1292c94c4ef22c6847d331645f00356afac3f4b5db5129",
    "tipblock_height" : 286342,
    "synblock_height" : 11323937,
    "finblock_height" : 0,
    "finblock_hash" : "0000000000000000000000000000000000000000000000000000000000000000",
    "connections" : 8,
    "errors" : "",
    "state" : "IBD"
}
root@6594c134fb1f:/opt/wicc#
```

If the return value is the same as above, the Mainnet node is successfully built.

<br>

---
## Linux (Ubuntu/CentOS)
---

<br>

refer to [build locally](./build.md) And [WaykiChain.conf](./conf.md)

### The local directory `/opt/wicc` is structured as follows
```
root@ubuntu:/opt/wicc# ls
coind  WaykiChain 
```

### The local directory `~/.WaykiChain ` is structured as follows
```
root@ubuntu:/opt/wicc# ls ~/.WaykiChain
 WaykiChain.conf
 ```

#### Configuration file content `WaykiChain.conf`
```
#nettype=main|test|regtest
nettype=main
rpcserver=1
rpcallowip=0.0.0.0/0
rpcport=6968
rpcuser=wiccuser
rpcpassword=wicc1000
rpcthreads=8
logprinttoconsole=0
logprinttofile=1
logprintfileline=1
logtimestamps=1
listen=1
disablesafemode=1
genblock=0
genblocklimit=1000000
debug=INFO
```

### Start node
command
```
./coind &
```

### Check if the node is working properly

After the node starts, run the following command to confirm whether the node is running normally.
```
./coind getinfo
```

**example**

```
root@ubuntu:/opt/wicc# ./coind getinfo
{
    "version" : "v3.2.0.1-78dc7178-release-linux (2020-08-04 18:52:02 +0800)",
    "protocol_version" : 10001,
    "net_type" : "MAIN_NET",
    "proxy" : "",
    "public_ip" : "116.25.40.216",
    "conf_dir" : "/root/.WaykiChain/WaykiChain.conf",
    "data_dir" : "/root/.WaykiChain/main",
    "block_interval" : 10,
    "genblock" : 0,
    "time_offset" : 0,
    "WICC_balance" : 0,
    "WUSD_balance" : 0,
    "WGRT_balance" : 0,
    "relay_fee_perkb" : 1000,
    "tipblock_tx_count" : 2,
    "tipblock_fuel_rate" : 1,
    "tipblock_fuel" : 10,
    "tipblock_time" : 1528964950,
    "tipblock_hash" : "80c0d33440cb92168f1292c94c4ef22c6847d331645f00356afac3f4b5db5129",
    "tipblock_height" : 286342,
    "synblock_height" : 11323937,
    "finblock_height" : 0,
    "finblock_hash" : "0000000000000000000000000000000000000000000000000000000000000000",
    "connections" : 8,
    "errors" : "",
    "state" : "IBD"
}
root@ubuntu:/opt/wicc#
```
If the return value is the same as above, the Mainnet node is successfully built.


[1]:	./build.md
[2]:	./conf.md

<br>

---
