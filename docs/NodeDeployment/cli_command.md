### CMD Line interface

Here we will learn how to use the command line interface (CMD Line) to interact with the node.


#### 1.Start the node

First off you have to have your node running. See Installing a Node to you haven't done so already. 

<br>

---

## `coind` command 

The `coind` command allows us to interact with the node. It is the basic starting point and used in combination with other commands. The example below shows us how to get help and learn about all the functions of the RPC.

<br>

---

## `coind help`

The `coind help` to view all of the available functions for the json RPC

`coind help`

```bash
root@desktopk: coind help
root@0144e839e084:/opt/wicc# coind help
addnode "node:port" "add|remove|onetry"
backupwallet "dest_dir"
createmulsig num_signatures ["address",...]
decodetxraw "hexstring"
disconnectblock "numbers"
dropminermainkeys
dropprivkey "address"
droptxfrommempool "txid"
dumpdb "[key_prefix_type]" "[file_path]"
dumpprivkey "address"
dumpwallet "filename"
encryptwallet "passphrase"
gendexoperatorordertx "sender" "order_type" "order_side" "symbol:coins:unit" "symbol:assets:unit" price dex_id "open_mode" taker_fee_ratio maker_fee_ratio "[symbol:fee:unit]" "[symbol:operator_tx_fee:unit]" "[memo]"
genunsignedtxraw "func" "params"
genutxomultiinputcondhash "n" "m" "pre_utxo_txid" "pre_utxo_tx_vout_index" "signee_uids","spend_txuid"
genutxomultisignaddr "n" "m" "signee_uids"
genutxomultisignature "sender" "utxo_content_hash"
getaccountinfo "addr"
getaddednodeinfo "dns" ["node"]
getassetinfo $asset_symbol
getblock "hash or height"  ["list_txs"] ["verbose"]
getblockcount
getblockfailures "block height"
getblockundo "hash or height"
getcdpinfo $cdp_id
getcdpparam $bcoin_scoin_pair $param_name
getchaininfo "count" [height]
getclosedcdp "[cdp_id | close_txid]"
getcoinunitinfo
getconnectioncount
getcontractaccountinfo "contract_regid" "account address or regid"
getcontractassets "contract_regid"
getcontractdata "contract_regid" "key" [hexadecimal]
getcontractinfo "contract regid"
getcontractregid
getdexbaseandquotecoins
getdexoperator dex_id
getdexoperatorbyowner owner_addr
getdexorder "order_id"
getdexorderfee "account_addr" "tx_type" ["fee_symbol"]
getfeedcoinpairs
getgovernors
gethash  "str"
getinfo
getminedblocks
getminerbyblocktime height time
getmininginfo
getnettotals
getnetworkinfo
getnewaddr  ["IsMiner"]
getpeerinfo
getproposal "proposalid"
getrawmempool ( verbose )
getscoininfo
getsignature
getswapcoin "peer_chain_coin_symbol"
getsysparam $param_name
gettotalbpssize
gettotalcoins
gettxdetail "txid"
getusercdp "cdp_owner_addr"
getwalletinfo
help ( "command" )
importprivkey "privkey"
importwallet "filename"
invalidateblock "hash"
listaddr
listassets
listcontractassets regid
listcontracts "show detail"
listdelegates
listdexorders ["begin_height"] ["end_height"] ["max_count"] ["last_pos_info"]
listdexsysorders ["height"]
listmintxfees
listtx
listtxcache
luavm_executecontract "addr" "app_id" ["arguments"] [symbol:amount:unit]
luavm_executescript "addr" "script_path" ["arguments"] [amount] [symbol:fee:unit]
ping
reconsiderblock "hash" [children]
reloadtxcache
saveblocktofile "blockhash" "filepath"
setgenerate generate ( genblocklimit )
signmessage "signee" "message"
signtxraw "str" "addr"
startcommontpstest "period" "batch_size"
startcontracttpstest "regid" "period" "batch_size"
startwasmtpstest "regid" "period" "batch_size"
stop
submitaccountpermproposal "sender" "account_uid" "proposed_perms_sum" ["fee"]
submitaccountpermscleartx "addr"  ["fee"]
submitaccountregistertx "addr" ["fee"]
submitassetpermproposal "sender" "asset_symbol" "proposed_perms_sum" ["fee"]
submitaxccoinproposal "sender" "peer_coin_symbol" "peer_chain_type" "operate_type" ["fee"]
submitaxcinproposal "sender" "peer_chain_type" "peer_chain_token_symbol" "self_chain_token_symbol" "peer_chain_addr" "peer_chain_txid" "self_chain_uid" "swap_amount" ["fee"]
submitaxcoutproposal "sender" "self_chain_token_symbol" "peer_chain_addr" "swap_amount" ["fee"]
submitblock "hexdata" ( "jsonparametersobject" )
submitcancelorderproposal "sender"  "order_id" ["fee"]
submitcdpliquidatetx "sender" "cdp_id" liquidate_amount [symbol:fee:unit]
submitcdpparamgovernproposal "sender" "param_name" "param_value" "bcoin_symbol" "scoin_symbol" ["fee"]
submitcdpredeemtx "sender" "cdp_id" repay_amount redeem_amount ["symbol:fee:unit"]
submitcdpstaketx "sender" stake_combo_money mint_combo_money ["cdp_id"] [symbol:fee:unit]
submitcoinstaketx "sender" "coins_to_stake" ["action"] ["symbol:fee:unit"]
submitcointransferproposal $sender $from_uid $to_uid $transfer_amount [$fee]
submitdelegatevotetx "sendaddr" "votes" "fee" ["height"]
submitdexbuylimitordertx "sender" "coin_symbol" "symbol:asset_amount:unit"  price [dex_id] [symbol:fee:unit] "[memo]"
submitdexbuymarketordertx "sender" "coin_symbol" coin_amount "asset_symbol"  [dex_id] [symbol:fee:unit] "[memo]"
submitdexcancelordertx "sender" "txid" [symbol:fee:unit]
submitdexoperatorregtx  "sender" "owner_uid" "fee_receiver_uid" "dex_name" "portal_url" "open_mode" maker_fee_ratio taker_fee_ratio ["fees"] ["memo"]
submitdexopupdatetx  "sender" "dex_id" "update_field" "value" "fee"
submitdexselllimitordertx "sender" "coin_symbol" "asset" price [dex_id] [symbol:fee:unit] "[memo]"
submitdexsellmarketordertx "sender" "coin_symbol" "asset_symbol" asset_amount  [dex_id] [symbol:fee:unit] "[memo]"
submitdexsettletx "sender" "deal_items" [symbol:fee:unit]
submitdexswitchproposal "sender" "dexid" "operate_type" ["fee"]
submitdiaissueproposal "sender"  "asset_symbol" "owner_uid" "total_supply" ["fee"]
submitfeedcoinpairproposal "sender" "base_symbol" "quote_symbol" "operate_type" ["fee"]
submitgovernorupdateproposal "sender" "governor_uid" "operate_type" ["fee"]
submitluacontractcalltx "sender_addr" "contract_regid" "arguments" "amount" "fee" ["height"]
submitluacontractdeploytx "addr" "filepath" "fee" ["height"] ["contract_memo"]
submitminerfeeproposal "sender" "tx_type" "fee_info"  ["fee"]
submitparamgovernproposal "sender" "param_name" "param_value" ["fee"]
submitpasswordprooftx "sender" "utxo_txid" "utxo_vout_index" "password" "pre_utxo_tx_uid" ["fee"]
submitpricefeedtx "sender" {price_feeds_json} ["symbol:fee:unit"]
submitproposalapprovaltx "sender" "proposalid" ["fee"] ["axc_out_signature"]
submitsendmultitx "from" "to" "transfer_array" "fee" ["memo"]
submitsendtx "sender" "to" "symbol:coin:unit" ["symbol:fee:unit"] ["memo"]
submitsetcodetx "sender" "wasm_file" "abi_file" ["contract"] [symbol:fee:unit]
submittotalbpssizeupdateproposal "sender" "total_bps_size" "effective_height"  ["fee"]
submittx "sender" "contract" "action" "data" "fee"
submittxraw "rawtx" ["signatures"]
submitucontractcalltx "sender_addr" "contract_regid" "arguments" "symbol:coin:unit" "symbol:fee:unit" ["height"]
submitucontractdeploytx "sender" "filepath" "symbol:fee:unit" ["height"] ["contract_memo"]
submitutxotransfertx "sender" "coin_symbol" "vins" "vouts" "symbol:fee:unit" "memo"
validateaddr "address"
verifychain ( checklevel numofblocks )
verifymessage "address" "signature" "message"
walletlock
walletpassphrase "passphrase" "timeout"
walletpassphrasechange "oldpassphrase" "newpassphrase"
wasm_abidefjson2bin "abijson"
wasm_bin2json "contract" "action" "data"
wasm_getabi "contract"
wasm_getcode "contract"
wasm_getresult "contract" "action" "data"
wasm_getrow "contract" "table" "key"
wasm_gettable "contract" "table" "key_prefix" "max_count" "begin_key"
wasm_gettxtrace "trxid"
wasm_json2bin "contract" "action" "data"
root@0144e839e084:/opt/wicc#
```

<br>

---

## `coind help command name`

**Example**

**`coind help submitsendtx`**

```bash
root@261680edb51c:/opt/wicc# coind help submitsendtx
submitsendtx "sender" "to" "symbol:coin:unit" ["symbol:fee:unit"] ["memo"]

Send coins to a given address.

Arguments:
1."sender":              (string, required) The address where coins are sent from
2."to":                  (string, required) The address where coins are received
3."symbol:coin:unit":    (symbol:amount:unit, required) transferred coins
4."symbol:fee:unit":     (symbol:amount:unit, optional) fee paid to miner, default is WICC:10000:sawi
5."memo":                (string, optional)

Result:
"txid"                   (string) The transaction id.

Examples:
> ./coind submitsendtx "wLKf2NqwtHk3BfzK5wMDfbKYN1SC3weyR4" "wNDue1jHcgRSioSDL4o1AzXz3D72gCMkP6" "WICC:1000000:sawi" "WICC:10000:sawi" "Hello, WaykiChain!"

As json rpc call
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitsendtx", "params": ["wLKf2NqwtHk3BfzK5wMDfbKYN1SC3weyR4", "wNDue1jHcgRSioSDL4o1AzXz3D72gCMkP6", "WICC:1000000:sawi", "WICC:10000:sawi", "Hello, WaykiChain!"] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/

root@0144e839e084:/opt/wicc# 
```

The `coind help` + command to view the specific operation guide.

`coind help submitsendtx` Displays help information, including information on command functions, parameter meanings, return value meanings, examples, and more.

The help information is consistent with the RPC-JSON interface, which is convenient for guiding the developer to operate the node.

<br>

---

## `coind getinfo` 

get various state information.

**Arguments:**
Returns an object containing various state info.

**Examples:**

```bash

./coind getinfo

```

As json rpc

```
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "getinfo", "params": [] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

Results

```json

{
    "version" : "v3.2.0.1-78dc7178-release-linux (2020-08-04 18:52:02 +0800)",
    "protocol_version" : 10001,
    "net_type" : "REGTEST_NET",
    "proxy" : "",
    "public_ip" : "",
    "conf_dir" : "/root/.WaykiChain/WaykiChain.conf",
    "data_dir" : "/root/.WaykiChain/regtest",
    "block_interval" : 10,
    "genblock" : 1,
    "time_offset" : 0,
    "WICC_balance" : 0,
    "WUSD_balance" : 0,
    "WGRT_balance" : 0,
    "relay_fee_perkb" : 1000,
    "tipblock_tx_count" : 3,
    "tipblock_fuel_rate" : 100,
    "tipblock_fuel" : 0,
    "tipblock_time" : 1504305600,
    "tipblock_hash" : "ab8d8b1d11784098108df399b247a0b80049de26af1b9c775d550228351c768d",
    "tipblock_height" : 0,
    "synblock_height" : 0,
    "finblock_height" : 0,
    "finblock_hash" : "0000000000000000000000000000000000000000000000000000000000000000",
    "connections" : 0,
    "errors" : "",
    "state" : "InSync"
}
```

<br>

---

## `coind getnewaddr` 

get a new address

**Arguments:**

1. "IsMiner" (bool, optional) If true, it creates two sets of key-pairs: one for mining and another for receiving miner fees.

**Result:**

```json
{
    "addr" : "wgxSeWySX8UNtAZBsdyoPCayzpJcYvKnps",
    "minerpubkey" : "null"
}
```

**Examples:**

```
> ./coind getnewaddr
```

As json rpc

```
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "getnewaddr", "params": [] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

<br>

---

## `coind listaddr` 

View all addresses and balances in the node wallet.

return Array containing address, balance, haveminerkey, regid information.

**Result:**

```json

    {
        "addr" : "wUuDGJkeMg2ZLkGd5ZM7TyK56DHoPVVDYa",
        "regid" : "0-0",
        "regid_mature" : false,
        "received_votes" : 0,
        "tokens" : {
        },
        "hasminerkey" : false
    },
    {
        "addr" : "wg3MGv5ZY8zA44QSWXatMa3WhpJXu1APMj",
        "regid" : "0-0",
        "regid_mature" : false,
        "received_votes" : 0,
        "tokens" : {
        },
        "hasminerkey" : false
    }
```

**Examples:**

```
> ./coind listaddr
```

**As json rpc call**

```
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "listaddr", "params": [] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

<br>

---

## `coind submitsendtx` 

transfer address

submitsendtx "from" "to" "symbol:coin:unit" "symbol:fee:unit" ["memo"]

Send coins to a given address.

**Arguments:**

1. "from"                 (string, required) The address where coins are sent from.
2. "to"                   (string, required) The address where coins are received.
3. "symbol:coin:unit":    (symbol:amount:unit, required) transferred coins
4. "symbol:fee:unit":     (symbol:amount:unit, required) fee paid to miner, default is WICC:10000:sawi
5. "memo":                (string, optional)

**Result:**

"txid"                   (string) The transaction id.

**Examples:**

```
> ./coind submitsendtx "wLKf2NqwtHk3BfzK5wMDfbKYN1SC3weyR4" "wNDue1jHcgRSioSDL4o1AzXz3D72gCMkP6" "WICC:1000000:sawi" "Hello, WaykiChain!"
```

**As json rpc call**

```
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitsendtx", "params": ["wLKf2NqwtHk3BfzK5wMDfbKYN1SC3weyR4", "wNDue1jHcgRSioSDL4o1AzXz3D72gCMkP6", "WICC:1000000:sawi", "Hello, WaykiChain!"] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```


## `coind getaccountinfo`

get account information

**Arguments:**

1. "addr": (string, required) account base58 addressReturns account details.

**Result:**
```json
{
   "address" : "wg3MGv5ZY8zA44QSWXatMa3WhpJXu1APMj",
    "keyid" : "dfe89dd2d86c3b86ad5b79c425dfec53796dbef9",
    "regid" : "0-0",
    "regid_mature" : false,
    "owner_pubkey" : "03ada569b7938a1854a52522e536ae1001c812c6e0e89fb819530a1477a4970c73",
    "miner_pubkey" : "",
    "perms" : "1111111111",
    "tokens" : {
    },
    "received_votes" : 0,
    "vote_list" : [
    ],
    "onchain" : false,
    "in_wallet" : true,
    "pubkey_registered" : false
    "cdp_list" : [
    ]
}
```

**Examples:**

```
> ./coind getaccountinfo "WT52jPi8DhHUC85MPYK8y8Ajs8J7CshgaB"
```

**As json rpc call**

```
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "getaccountinfo", "params": ["WT52jPi8DhHUC85MPYK8y8Ajs8J7CshgaB"] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```


<br>

---
## Linux 
---
<br>

(1) Common operation

| command               |    description     |
|:----------------------|:------------------:|
| ./coind help          |   get help info    |
| ./coind getinfo       |   get node info    |
| ./coind listaddr      |  get address list  |
| ./coind getnewaddress | create new address |

<br>

### NodeDeployment Local directory ：

|File/Folder     |                                 meaning                                 |
| :--------------- | :--------------------------------------------------------------------: |
| coind            | executable file                                                        |
| WaykiChain.conf  | configuration                                                          |
| main             | data from mainnet                                                      |
| testnet          | data from testnet                                                      |
| peer.dat         | nodes connections info                                                 |
| wallet.dat       | wallet data **include public and private key**                          |
| blocks           | block data Folder                                                      |
| syncdata         | block synchronization data                                             |
| database         | will be deleted after the node running                                 |

<br>

#### Additional ways of running WaykiChain nodes

| running command            | meaning                                      |
| :-------------------------- | :--------------------------------------------: |
| ./coind -datadir=.         | Configuration file in current directory      |
| ./coind -datadir=/opt/wicc | Configuration file in directory /opt/wicc    |
| ./coind                    | Configuration file in current user directory:\~/.WaykiChain |


**Note**

```
./coind -datadir=.      //correct command
./coind -datadir =.     //error command
```

<br>


### 3. Introduction to each file in the node wallet:

| File/folder     |                                 Description                                 |
|:--------------- |:--------------------------------------------------------------------:|
| coind           |                              executable file                              |
| WaykiChain.conf |                               Configuration file                               |
| main            |                           Official chain data folder                           |
| testnet         |                           Test chain data folder                           |
| peer.dat        |  Located in the data folder, node connection information                   |
| wallet.dat      | Located in the data folder, wallet data, this file stores the wallet public and private keys, must be kept |
| blocks          |                Synchronized block data folder located in the data folder                |
| syncdata        |                 Located in the data folder, synchronize the situation data folder                 |
| database        |  Located in the data folder, the node program will be started when it is started, and the node program will be deleted automatically after it stops.  |


[1]:	../DeveloperHelper/faucet.md
[2]:	../JsonRpcApi/account.md
[3]:	../JsonRpcApi/account.md#listaddr
[4]:	../JsonRpcApi/tx.md#sendtoaddress
[5]:	../JsonRpcApi/account.md#listaddr
[6]:	../JsonRpcApi/account.md#getaccountinfo

<br>

---
