# cold wallet-kotlin

--- 
## Pull code 
```
// Download by CMD
git clone https://github.com:WaykiChain/wicc-wallet-utils-kotlin.git

// Download by https
https://github.com/WaykiChain/wicc-wallet-utils-kotlin
```

## Build
### IntelliJ IDEA
check local jdk version by use CMD
``` 
java -version
```
it needn't modify if the jdk version in local is 8, and if that is 14, then should set `distributionUrl` value as follows in directory `gradle`:`wrapper`:`gradle-wrapper.properties`
```
distributionUrl=https\://services.gradle.org/distributions/gradle-4.8.1-bin.zip
```

## Create private key (include wallet address and public key)
**Please refer to : src/test/kotlin/com/waykichain/wallet/TestWallet.kt**
### 1.GenerateMnemonics. You will get 12 words
```
// Resquest:
var words = MnemonicUtil.randomMnemonicCodes()
logger.info(words.toString())
  
//Respond
info: [vote, image, poverty, list, book, siren, dentist, cave, north, right, suit, liberty]
```

### 2.generate wallet from mnemonic
```
// Request
val words = "vote, image, poverty, list, book, siren, dentist, cave, north, right, suit, liberty"
val networkParameters = WaykiTestNetParams.instance //generate Testnet Address From Mnemonic
//val networkParameters = WaykiMainNetParams.instance //generate Mainnet Address From Mnemonic
val wallet= BIP44Util.generateWaykiWallet(words, networkParameters)
logger.info("\nmnemonic: $words\naddress:   ${wallet.address} \n Private key: ${wallet.privateKey} \nPublic Key: ${wallet.pubKey}")

// Respond
info: 
mnemonic: vote, image, poverty, list, book, siren, dentist, cave, north, right, suit, liberty
address:   waMDJew9pm5e6fT7aCaWG1yBwcPx1mW7B2 
Private key: Y9wDyMys64KVhqwAVxbAB4aYDNVQ4HpRhQ7FLWFC3MhNNXz4JHot 
Public Key: 02a89bd7e93f119c37a02c3397ca153c22c9631677acd1fa9cd77ea5c20f4e8f6d
```

### 3.Import private key
```
// Resquest
val params = WaykiTestNetParams.instance //TestNet
val privKeyWiF = “Y9wDyMys64KVhqwAVxbAB4aYDNVQ4HpRhQ7FLWFC3MhNNXz4JHot”
val key = DumpedPrivateKey.fromBase58(params, privKeyWiF).key
logger.info(Utils.HEX.encode(key.pubKeyHash))

// Respond
info: 94ed078235cdfdb52e35c2fdf4cee77e286361fa
```

## WaykiChain Sign Transaction
**Signing a transaction with a private key,you can submit your offline signature rawtx transaction by bass service.**


|  BassNetwork |  ApiAddr | 
|-------------- |----------------------------------|
|   TestNetwork | https://baas-test.wiccdev.org/v2/api/swagger-ui.html#!/  |  
|   ProdNetwork | https://baas.wiccdev.org/v2/api/swagger-ui.html#!/       | 

**Submit raw string:**  
Mainnet <https://baas.wiccdev.org/v2/api/swagger-ui.html#!/transaction-controller/offlinTransactionUsingPOST> ,  
TestNet <https://baas-test.wiccdev.org/v2/api/swagger-ui.html#!/transaction-controller/offlinTransactionUsingPOST>  

**Get block height:**  
MainNet<https://baas.wiccdev.org/v2/api/swagger-ui.html#!/block-controller/getBlockCountUsingPOST>,  
TestNet <https://baas-test.wiccdev.org/v2/api/swagger-ui.html#!/block-controller/getBlockCountUsingPOST>


## WaykiChain Transaction
**Please refer to: src/test/kotlin/com/waykichain/wallet/TestTransaction.kt**

### 1.Sign Register Account Transaction
**The register transaction is not required, you can activate wallet by public key in other transactions**
```
// Resquest
val wallet = LegacyWallet()
val netParams = WaykiTestNetParams.instance
val privKeyWiF = "Y9XMqNzseQFSK32SvMDNF9J7xz1CQmHRsmY1hMYiqZyTck8pYae3"
val key = DumpedPrivateKey.fromBase58(netParams, privKeyWiF).key
val txParams = WaykiRegisterAccountTxParams(key.publicKeyAsHex, null, 429821, 10000, CoinType.WICC.type)
txParams.signTx(key)
val tx = wallet.createRegisterTransactionRaw(txParams)
logger.info("${tx.length} - $tx")

// Respond
信息: 230 - 0201999c7d2102a722a3a94fb41d92bcf9d54cd76ea40c8b0c223d6f0570389b775120c5e487640083e1ac0046304402205304902f6ae8470e7c294b8abe7fdd5a9847d8980914234c9ddb9b6098e473d002200ad2d0238292285394447905cb20b7275cd2daf3a68d1237a1200982b99172bc
```

### 2.Sign Common Transaction
```
// Resquest
val wallet = LegacyWallet()
val netParams = WaykiTestNetParams.instance
val srcPrivKeyWiF = "Y6J4aK6Wcs4A3Ex4HXdfjJ6ZsHpNZfjaS4B9w7xqEnmFEYMqQd13"
val srcKey = DumpedPrivateKey.fromBase58(netParams, srcPrivKeyWiF).key
val pubKey = srcKey.publicKeyAsHex  //user public key 
val destAddr = "wWTStcDL4gma6kPziyHhFGAP6xUzKpA5if"
val memo="test transfer"
val txParams = WaykiCommonTxParams(WaykiNetworkType.TEST_NET, 34550, pubKey,10000,1100000000000, "0-1", destAddr,memo)
txParams.signTx(srcKey)
val tx = wallet.createCommonTransactionRaw(txParams)

```
`WaykiCommonTxParams` prase function
```
WaykiCommonTxParams(networkType: WaykiNetworkType, nValidHeight: Long,pubKey:String, fees: Long, val value: Long, val srcRegId: String, destAddr: String,val memo: String)
```

|param|func|
|--------|-------|
|networkType|connect type : <br> TestNet:WaykiNetworkType.TEST_NET<br> MainNet：WaykiNetworkType.MAIN_NET|
|nValidHeight|valid height|
|pubKey|public key|
|fees|charge with miner|
|value|transfer charge|
|srcRegId|sender regid|
|destAddr|receive address|
|memo| |


## How to build

### Execution Command

`gradle jar -PwiccBuildJar`

### output

`build/libs/wicc-wallet-utils-2.0.0.jar `

## Reference Projects

https://bitcoinj.github.io/  
https://github.com/bitcoin/secp256k1
