# common submit order

---

## instruction manual   

** maker order：** Take the maker orders by initiative, waiting for a deal  

** taker order：** deal with other‘s maker orders by initiative  

How to judge maker order or taker order:  
1、limit order and limit order: it depend on the  order of insert chain block  
2、limit order and market order: market order stand for taker order, marsket order stand for market order  
3、market order and market order:  small probability event,  taker order is the latter order  


## submitdexselllimitordertx  
submit limit order with sell

**Parameters**
submitdexselllimitordertx "sender" "coin_symbol" "asset" price [dex_id] [symbol:fee:unit] "[memo]"

`sender`                    (string required) the tx sender's address   

`coin_symbol`          (string required) coin type to get    

`asset_symbol:asset_amount:unit`   (comboMoney,required) the target amount to sell.    default symbol is WICC, default unit is sawi.   

`price`                      (numeric, required) bidding price willing to sell  

`dex_id`                    (numeric, optional) Decentralized Exchange(DEX) ID, default is 0   

`symbol:fee:unit` (string:numeric:string, optional) fee paid for miner, default is WICC:10000:sawi   

`memo`                        (string, optional)  memo    

**Result**

`txid`            (string) The transaction id.   

**Examples**
```
\\ Request
root@0e7612ac5bc5:/opt/wicc# coind submitdexselllimitordertx "0-9" "WUSD" "WICC:1000000000:sawi" 1000000000 0

\\ Respond
{
    "txid" : "2ddc416bfce926cd5cb286deb3de296b7c5df82dc2673e1cf82b9f3cd8491754"
}

\\ As json rpc call
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitdexselllimitordertx", "params": ["10-3", "WUSD", "WICC:1000000000:sawi", 100000000, 0
] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/

```

---

## submitdexbuylimitordertx
submit limit order by buy

**Parameters**
submitdexbuylimitordertx "sender" "coin_symbol" "asset" price [dex_id] [symbol:fee:unit] "[memo]"

`sender`                    (string required)  the tx sender's address   

`coin_symbol`          (string required) coin type to pay  

`asset_symbol:asset_amount:unit`   (comboMoney,required) the target amount to buy
default symbol is WICC, default unit is sawi.   

`price`                      (numeric, required) bidding price willing to buy  

`dex_id`                    (numeric, optional) Decentralized Exchange(DEX) ID, default is 0   

`symbol:fee:unit` (string:numeric:string, optional) fee paid for miner, default is WICC:10000:sawi   

`memo`                        (string, optional)  memo    

**Result**

`txid`            (string) The transaction id.  


**Examples**
```
\\ Request
root@0e7612ac5bc5:/opt/wicc# coind submitdexbuylimitordertx "0-1" "WICC" "WUSD:1000000000000:sawi" 1000000000 1

\\ Respond
{
    "txid" : "bbdb87261018a4bf8f3407036a8a2e34de1dad863be00c0557b554e57c341bdb"
}

\\ As json rpc call
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitdexbuylimitordertx", "params": ["10-3", "WUSD", "WICC:1000000000:sawi", 100000000, 0
] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/

```

---

## submitdexsellmarketordertx
submit market order by sell

**Parameters**
submitdexsellmarketordertx "sender" "coin_symbol" "asset_symbol" asset_amount  [dex_id] [symbol:fee:unit] "[memo]"  

`sender`                    (string required) the tx sender's address 

`coin_symbol`          (string required) coin type to get

`asset_symbol:asset_amount:unit`   (comboMoney,required) the target amount to sell, default symbol is WICC, default unit is sawi.  

`dex_id`                    (numeric, optional) Decentralized Exchange(DEX) ID, default is 0  

`symbol:fee:unit` (string:numeric:string, optional) fee paid for miner, default is WICC:10000:sawi  

`memo`                        (string, optional)  memo  

**Result**  

`txid`            (string) The transaction id.  


**Examples**  
```
\\ Request
root@0e7612ac5bc5:/opt/wicc# coind submitdexsellmarketordertx "0-1" "WUSD" "WICC:200000000:sawi" 0

\\ Respond
{
    "txid" : "636de35e645c38a1b61bab754ef86bad24a7c72c1747a5119cd2d02ed173ea5a"
}

\\ As json rpc call
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitdexsellmarketordertx", "params": ["10-3", "WUSD", "WICC:200000000:sawi", 1
] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```
---

## submitdexbuymarketordertx
submit market order by buy   

**Parameters**
submitdexbuymarketordertx "sender" "coin_symbol" coin_amount "asset_symbol"  [dex_id] [symbol:fee:unit] "[memo]"

`sender`                    (string required) the tx sender's address  

`coin_symbol:coin_amount:unit`          (string required)  the target coin amount for buying asset, default symbol is WUSD, default unit is sawi.  

`asset_symbol`       (comboMoney,required)  asset type to buy    

`dex_id`                    (numeric, optional) Decentralized Exchange(DEX) ID, default is 0   

`symbol:fee:unit` (string:numeric:string, optional) fee paid for miner, default is WICC:10000:sawi  

`memo`                        (string, optional)  memo     

**Result**  

`txid`            (string) The transaction id.  


**Examples**  
```
\\ Request
root@0e7612ac5bc5:/opt/wicc# coind submitdexbuymarketordertx "0-1" "WUSD:200000000:sawi"  "WICC" 1

\\ Respond
{
    "txid" : "636de35e645c38a1b61bab754ef86bad24a7c72c1747a5119cd2d02ed173ea5a"
}

\\ As json rpc call
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitdexbuymarketordertx", "params": ["10-3", "WUSD:200000000:sawi", "WICC", 1
] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```
---

