# WRC30

## Introduction
`WRC30` is a Token standard for digital assets based on the underlying wiki chain v3.0. Will replace the `WRC20` contract token standard

## Features
1. Token standard based on the development of the underlying public chain

2. Support for calling Lua contract as payment currency

3. Support trading on WikiLeaks Decentralized Exchange [WaykiDEX] (https://dex.waykichain.com/)

4. More developer-friendly, easier to issue and transfer development process

## Issuing Asset Expenses and Rules Description

1. Asset issuance fee: 550 WICC

2. The asset issuer and asset owner must be [regid mature] (../../Summary/account/)

---

## WRC30 Asset Issue

### One-click release of WRC30 digital assets via [Wiki Times] (https://www.waykichain.com/WaykiTimes.html)

![](../images/wrc30.png)

---

### Issuing WRC30 digital assets via RPC interface

#### 1. Use the RPC interface submitassetissuetx to issue WRC30 digital assets
Asset issuance fee : 550 WICC

Transaction fee: Refer to [charge list] (../../Summary/transaction/)

**Parameters**

`addr` transaction originator, different from asset owner

`asset_symbol` asset symbol 6~7 [A-Z] characters

`asset_owner_addr` asset owner address

`asset_name` asset name 1~32 random characters

`total_supply` Asset circulation, maximum: 900 x 10^8 x 10^8 Unit `sawi`

Whether the `mintable` asset can be incremented by `true` or `false`

**Returns**

`txid` asset issuance transaction hash

**Example**
```json
// Request
curl -u Waykichain:admin -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitassetissuetx", "params": ["wXwqeA9cftNS6fr71hUNDAzs92i1kgCuW7", "ABCDEFG", "wXwqeA9cftNS6fr71hUNDAzs92i1kgCuW7" , "abcdefg123", 1000000000000000, true] }' -H 'content-type:application/json;' http://127.0.0.1:6967

// Response
{
    "result": {
        "txid": "44d53e80152af957040c09de003a24b74836822e9c56a6f13375c0e74803fea0"
    },
    "error": null,
    "id": "curltest"
}
```

#### 2. Confirm whether the asset was successfully issued
Call the RPC interface [getaccountinfo](../../JsonRpcApi/account/#getaccountinfo) Check if the asset owner tokens list has added the issued token

Or call the RPC interface `getasset` to query the issued token information.

**Parameters**

`asset_symbol` asset symbol, 6~7 [A-Z] characters

**Returns**

`asset_symbol` asset symbol

`owner_uid` asset owner uid can be ignored

`owner_addr` asset owner address

`asset_name` asset name

`total_supply` Total assets issued in units of `sawi`

Whether the `mintable` asset can be incremented by `true` or `false`

`max_order_amount` DEX related parameters, not enabled

`min_order_amount` DEX related parameters, not enabled

```json
// Request
curl -u Waykichain:admin -d '{"jsonrpc": "1.0", "id":"curltest", "method": "getasset", "params": ["ABCDEFG"] }' -H 'content- Type:application/json;' http://127.0.0.1:6967

// Response
{
    "result": {
        "asset_symbol": "ABCDEFG",
        "owner_uid": "0-1",
        "owner_addr": "wXwqeA9cftNS6fr71hUNDAzs92i1kgCuW7",
        "asset_name": "abcdefg123",
        "total_supply": 1000000000000000,
        "mintable": true,
        "max_order_amount": 0,
        "min_order_amount": 0
    },
    "error": null,
    "id": "curltest"
}
```

#### 3. Use the RPC interface [submitsendtx](../../JsonRpcApi/tx/#submitsendtx) for WRC30 asset transfer

```json
// Request
curl -u Waykichain:admin -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitsendtx", "params": ["wXwqeA9cftNS6fr71hUNDAzs92i1kgCuW7", "wNDue1jHcgRSioSDL4o1AzXz3D72gCMkP6", "ABCDEFG: 10000:sawi", "WICC:1000000:sawi", "Hello, WaykiChain!"] }' -H 'content-type:application/json;' http://127.0.0.1:6967

// Response
{
    "result": {
        "txid": "61860923b2fbfc90e749a1c756495ac8ae7c33c9b24e9f64c31c5582ddafa646"
    },
    "error": null,
    "id": "curltest"
}
```

#### 4. Use the RPC interface [getaccountinfo](../../JsonRpcApi/account/#getaccountinfo) to see if the recipient account receives WRC30 assets.

```json
// Request
curl -u Waykichain:admin -d '{"jsonrpc":"2.0","id":"curltext","method":"getaccountinfo","params":["wNDue1jHcgRSioSDL4o1AzXz3D72gCMkP6"]}' -H 'content- Type:application/json;' http://127.0.0.1:6967

// Response
{
    "result": {
        "address": "wNDue1jHcgRSioSDL4o1AzXz3D72gCMkP6",
        "keyid": "1c758724cc60db35dd387bcf619a478ec3c065f2",
        "nickid": "",
        "regid": "37574-2",
        "regid_mature": true,
        "owner_pubkey": "0376de6a21f63c35a053c849a339598016a0261d6bdc5567adeda0af78b750c4cc",
        "miner_pubkey": "",
        "tokens": {
            "ABCDEFG": {
                "free_amount": 10000,
                "staked_amount": 0,
                "frozen_amount": 0,
                "voted_amount": 0
            }
        },
        "received_votes": 0,
        "vote_list": [],
        "position": "inblock",
        "cdp_list": []
    },
    "error": null,
    "id": "curltext"
}
```
