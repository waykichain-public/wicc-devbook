#### API's about block data

<br>

---

## getinfo

Returns an object containing various state info.

**Parameters**
* none

**Returns**

`version` the node version

`protocol_version` the protocol version

`net_type` The type of network environment the node is running on

`proxy` Node proxy address: port

`public_ip` Get the external ip address of the node.

`conf_dir` Directory path to the configuration file

`data_dir` Directory path for block data

`block_interval` Block time interval

`genblock` Whether to produce blocks

`time_offset` Current node time offset, unit: s

`WICC_balance` balance of WICC

`WUSD_balance` balance of WUSD

`WGRT_balance` balance of WGRT

`relay_fee_perkb` Recurring transaction fee / KB threshold

`tipblock_tx_count` the deal nums for the latest block in the node

`tipblock_fuel_rate` Fuel rate for the latest block in the node

`tipblock_fuel` Fuel cost of the latest block in the node

`tipblock_time` The block timestamp of the latest block in the node

`tipblock_hash` The hash value of the latest block in the node

`tipblock_height` The height of the latest block in the node

`syncblock_height` The latest block height of the whole network

`finblock_height`  The height of the confirmed block

`finblock_hash` The hex of the confirmed block

`connections` The number of bidirectional connections of the current node network,

`errors` Node operation error, warning message

`state`  The synchronous state of the current chain
```
Loading:    In import
ReIndexing: Reindexing in progress
IBD:        The status of initial block download
InSync:     Synchronized to the latest block
```
**Example**

```json
// Request
> coind getinfo. 

// Response
{
    "version" : "v3.0.0.1-79fe51df-release-linux (2020-07-09 11:10:35 +0800)",
    "protocol_version" : 10001,
    "net_type" : "REGTEST_NET",
    "proxy" : "",
    "public_ip" : "",
    "conf_dir" : "/root/.WaykiChain/WaykiChain.conf",
    "data_dir" : "/root/.WaykiChain/regtest",
    "block_interval" : 10,
    "genblock" : 1,
    "time_offset" : 0,
    "WICC_balance" : 0,
    "WUSD_balance" : 0,
    "WGRT_balance" : 0,
    "relay_fee_perkb" : 1000,
    "tipblock_tx_count" : 3,
    "tipblock_fuel_rate" : 100,
    "tipblock_fuel" : 0,
    "tipblock_time" : 1504305600,
    "tipblock_hash" : "ab8d8b1d11784098108df399b247a0b80049de26af1b9c775d550228351c768d",
    "tipblock_height" : 0,
    "synblock_height" : 0,
    "finblock_height" : 0,
    "finblock_hash" : "0000000000000000000000000000000000000000000000000000000000000000",
    "connections" : 0,
    "errors" : "",
    "state" : "InSync"
}
```

**How to judge whether the finalize is in block chain**
```
1. Find confirmed_height(H1) by using coind gettxdetail $txid
2. Find finblock_height(H2) by using coind getinfo
3. If H1 <= H2, it certify finalize is in block chain
```
<br>

---


## getblockcount

Get the number of blocks in the current node

**Parameters** 

none

**Returns**

`result` the number of blocks in the current node

**Example**

```json
// Request
curl -u Waykichain:admin -d '{"jsonrpc":"2.0","id":"curltext","method":"getblockcount"}' -H 'content-type:application/json;' http://127.0.0.1:6967

// Response
{
  "result":1284292,
  "error": null,
  "id": "curltext"
}
```

<br>

---


## getblock

Get block information about the block with the given hash orindex.

**Parameters**

`height`/`hash` block height/block hash


**Returns**

`curr_block_hash`  The hash of the cur block

`prev_block_hash`  The hash of th parent block

`next_block_hash`  The hash of the next block

`bp_uid`  The user ID of the producer

`version`  The block version

`merkle_root` The merkle root

`total_fuel_fee` the total fee of fuel

`confirmations` block confirmations

`size`  The block size, unit is byte

`height`  The block height or index

`tx_count` The transaction number

`tx` The transaction id

`time` The block time in seconds since epoch (Jan 1 1970 GMT)

`nonce` The nonce

`median_price` Median price list for stable currency system feed

```
`coin_symbol` currency symbol 
`price_symbol` on the standard currency 
`price` currency
```

`receipts` Block receipt list

**Example**

```json
// Request
curl -u Waykichain:admin -d '{"jsonrpc": "1.0", "id":"curltest", "method": "getblock", "params": [1000] }' -H 'content-type:application/json;' http://127.0.0.1:6967

// Response
{
    "curr_block_hash" : "5131e0510471018ef47b964f8775e7003a1067460ecf5e8920aaf6368d756130",
    "prev_block_hash" : "76e2c2a13c29af6cee3f678575924d6db672f0ea97f658911ba0ac7a7c8a1f17",
    "next_block_hash" : "5e9b615ab64bd5fa6963aedb1191f7999cdfd5886a50605bbb865fb5a27d2f61",
    "bp_uid" : "0-10",
    "version" : 1,
    "merkle_root" : "0b007a8efceef83b4e4de68b1e7bc50dceb5652ecbf894f520e9121a440ee8aa",
    "total_fuel_fee" : 0,
    "confirmations" : 13,
    "size" : 203,
    "height" : 98,
    "tx_count" : 2,
    "tx" : [
        "4fbdeb061b8d9b8edc2c07cb15b3f2b280f6138c6cdffd7d63f7d463a80be627",
        "7c283b57d57801bf8ba0d27bcb2f4fa23a8419dc4c8f159e67864dc4f45f4bbc"
    ],
    "time" : 1593864322,
    "nonce" : 101,
    "median_price" : [
    ],
    "receipts" : [
    ]
}
```

---

