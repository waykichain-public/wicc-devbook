DEX usage

Dex stands for Decentralized Exchange. Were going to start with the basics which is to buy and sell. 
We will start with buy. We will place a buy limit order. The buy limit order allows us to write the exact price we want to buy for.
 
<br>

---

## `submitdexbuylimitordertx`

##### Buy - limit order

submit a dex buy limit price order tx.

* Function: `submitdexbuylimitordertx`

* Usage: `submitdexbuylimitordertx "addr" "coin_symbol" "asset_symbol" asset_amount price [symbol:fee:unit]`

**Arguments**:

1. "addr": (string required) order owner address
2. "coin_symbol": (string required) coin type to pay
3. "asset_symbol": (string required), asset type to buy
4. "asset_amount": (numeric, required) amount of target asset to buy
5. "price": (numeric, required) bidding price willing to buy
6."symbol:fee:unit":(string:numeric:string, optional) fee paid for miner, default is WICC:10000:sawi

Result:
"txid" (string) The transaction id.

Examples:
```bash 
./coind submitdexbuylimitordertx "10-3" "WUSD" "WICC" 1000000 200000000`
```

As json rpc call
``` bash
 curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitdexbuylimitordertx", "params": ["10-3" "WUSD" "WICC" 1000000 200000000
] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

<br>

---

## `submitdexselllimitordertx`

##### Buy - limit order

* Function: `submitdexselllimitordertx`

* Usage: `submitdexselllimitordertx "addr" "coin_symbol" "asset_symbol" asset_amount price [symbol:fee:unit]`

submit a dex buy limit price order tx.

**Arguments**:

1. "addr": (string required) order owner address
2. "coin_symbol": (string required) coin type to pay
3. "asset_symbol": (string required), asset type to buy
4. "asset_amount": (numeric, required) amount of target asset to buy
5. "price": (numeric, required) bidding price willing to buy
6. "symbol:fee:unit":(string:numeric:string, optional) fee paid for miner, default is WICC:10000:sawi

Result:
"txid" (string) The transaction id.

Examples:
```bash 
 ./coind submitdexselllimitordertx "10-3" "WUSD" "WICC" 1000000 200000000
```

As json rpc call
```bash
 curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitdexselllimitordertx", "params": ["10-3" "WUSD" "WICC" 1000000 200000000
] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

<br>

---

## `submitdexbuymarketordertx`

##### Buy - Market Price 

* Function: `submitdexbuymarketordertx`

* Usage: `submitdexbuymarketordertx "addr" "coin_symbol" coin_amount "asset_symbol" [symbol:fee:unit]`

submit a dex buy market price order tx.

**Arguments**:

1. "addr": (string required) order owner address
2. "coin_symbol": (string required) coin type to pay
3. "coin_amount": (numeric, required) amount of target coin to buy
4. "asset_symbol": (string required), asset type to buy
5. "symbol:fee:unit":(string:numeric:string, optional) fee paid for miner, default is WICC:10000:sawi

Result:
"txid" (string) The transaction id.

Examples:
```bash
 ./coind submitdexbuymarketordertx "10-3" "WUSD" 200000000 "WICC"
```

As json rpc call
```bash
 curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitdexbuymarketordertx", "params": ["10-3" "WUSD" 200000000 "WICC"
] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

<br>

---

## `submitdexsellmarketordertx`

##### Sell - Market Price

* Function: `submitdexsellmarketordertx`
* Usage: `submitdexsellmarketordertx "addr" "coin_symbol" "asset_symbol" asset_amount [symbol:fee:unit]`

submit a dex sell market price order tx.

**Arguments**:

1. "addr": (string required) order owner address
2. "coin_symbol": (string required) coin type to pay
3. "asset_symbol": (string required), asset type to buy
4. "asset_amount": (numeric, required) amount of target asset to buy
5. "symbol:fee:unit":(string:numeric:string, optional) fee paid for miner, default is WICC:10000:sawi

Result:
"txid" (string) The transaction id.

Examples:
```bash
 ./coind submitdexsellmarketordertx "10-3" "WUSD" "WICC" 200000000
```

As json rpc call
```bash
 curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitdexsellmarketordertx", "params": ["10-3" "WUSD" "WICC" 200000000
] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

<br>

---

## `getdexorder`

##### DEX Order Information 

* Function: `getdexorder`

* Usage: `getdexorder "order_id"`

get dex order detail.

**Arguments**:

1. "order_id": (string required) order txid

Result: object of order detail

**Examples:**

```bash
 ./coind getdexorder "c5287324b89793fdf7fa97b6203dfd814b8358cfa31114078ea5981916d7a8ac"
```

As json rpc call

```bash
 curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "getdexorder", "params": ["c5287324b89793fdf7fa97b6203dfd814b8358cfa31114078ea5981916d7a8ac" ] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

<br>

---

## `getdexorders`

##### All DEX orders
 
* Function: `getdexorders`

* Usage: `getdexorders ["begin_height"] ["end_height"] ["max_count"] ["last_pos_info"]`

get dex all active orders by block height range.

**Arguments**:

1. "begin_height": (numeric optional) the begin block height, default is 0
2. "end_height": (numeric optional) the end block height, default is current tip block height
3. "max_count": (numeric optional) the max order count to get, default is 500
4. "last_pos_info": (string optional) the last position info to get more orders, default is empty

Result:
"begin_height" (numeric) the begin block height of returned orders.
"end_height" (numeric) the end block height of returned orders.
"has_more" (bool) has more orders in db.
"last_pos_info" (string) the last position info to get more orders.
"count" (numeric) the count of returned orders.
"orders" (string) a list of system-generated DEX orders.

**Examples:**

```bash
 ./coind getdexorders 0 100 500
```

As json rpc call

```bash
 curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "getdexorders", "params": [0 100 500] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

<br>

---

## `getdexsysorders`

##### All System Generated DEX orders

* Function: `getdexsysorders`

* Usage: `getdexsysorders ["height"]`

get dex system-generated active orders by block height.

**Arguments**:

1. "height": (numeric optional) block height, default is current tip block height

**Result:**

"height" (string) the specified block height.
"orders" (string) a list of system-generated DEX orders.

**Examples:**

```bash
 ./coind getdexsysorders 10
```

As json rpc call

```bash
 curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "getdexsysorders", "params": [10] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

<br>

---

## `submitdexcancelordertx`

##### Cancel Order

* Function: `submitdexcancelordertx`
* Usage: `submitdexcancelordertx "addr" "txid" [symbol:fee:unit]`

submit a dex cancel order tx.

**Arguments**:

1. "addr": (string required) order owner address
2. "txid": (string required) order tx want to cancel
3. "symbol:fee:unit":(string:numeric:string, optional) fee paid for miner, default is WICC:10000:sawi

Result:
"txid" (string) The transaction id.

Examples:
```bash
 ./coind submitdexcancelordertx "WiZx6rrsBn9sHjwpvdwtMNNX2o31s3DEHH" "c5287324b89793fdf7fa97b6203dfd814b8358cfa31114078ea5981916d7a8ac"
```

As json rpc call
```bash
 curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitdexcancelordertx", "params": ["WiZx6rrsBn9sHjwpvdwtMNNX2o31s3DEHH" "c5287324b89793fdf7fa97b6203dfd814b8358cfa31114078ea5981916d7a8ac"] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

<br>

---

## `submitdexsettletx`

##### Settle Order

* Function: `submitdexsettletx`

* Usage: `submitdexsettletx "addr" "deal_items" [symbol:fee:unit]`

 submit a dex settle tx.
 
 **Arguments**:
 
 1. "addr": (string required) settle owner address
 2. "deal_items": (string required) deal items in json format
  [
      {
         "buy_order_id":"txid", (string, required) order txid of buyer
         "sell_order_id":"txid", (string, required) order txid of seller
         "deal_price":n (numeric, required) deal price
         "deal_coin_amount":n (numeric, required) deal amount of coin
         "deal_asset_amount":n (numeric, required) deal amount of asset
      }
          ,...
    ]
 3. "symbol:fee:unit":(string:numeric:string, optional) fee paid for miner, default is WICC:10000:sawi
 
 Result:
 "txid" (string) The transaction id.
 
 **Examples:**
 
```bash
 ./coind submitdexsettletx "WiZx6rrsBn9sHjwpvdwtMNNX2o31s3DEHH" "[{\"buy_order_id\":\"c5287324b89793fdf7fa97b6203dfd814b8358cfa31114078ea5981916d7a8ac\", \"sell_order_id\":\"c5287324b89793fdf7fa97b6203dfd814b8358cfa31114078ea5981916d7a8a1\", \"deal_price\":100000000,\"deal_coin_amount\":100000000,\"deal_asset_amount\":100000000}]"
```
 
As json rpc call
 
```bash
 curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitdexsettletx", "params": ["WiZx6rrsBn9sHjwpvdwtMNNX2o31s3DEHH" [{"buy_order_id":"c5287324b89793fdf7fa97b6203dfd814b8358cfa31114078ea5981916d7a8ac", "sell_order_id":"c5287324b89793fdf7fa97b6203dfd814b8358cfa31114078ea5981916d7a8a1", "deal_price":100000000,"deal_coin_amount":100000000,"deal_asset_amount":100000000}]] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

<br>

---
