

### Opening a CDP

* Function: `submitcdpstaketx`
* Usage: `submitcdpstaketx "addr" stake_combo_money mint_combo_money ["cdp_id"] [symbol:fee:unit]`

submit a CDP Staking Tx.

Arguments:

1. "addr":               (string, required) CDP Staker's account address
2. "stake_combo_money":  (symbol:amount:unit, required) Combo Money to stake into the CDP, default symbol=WICC, default unit=sawi
3. "mint_combo_money":   (symbol:amount:unit, required), Combo Money to mint from the CDP, default symbol=WUSD, default unit=sawi
4. "cdp_id":             (string, optional) CDP ID (tx hash of the first CDP Stake Tx)
5. "symbol:fee:unit":    (symbol:amount:unit, optional) fee paid to miner, default is WICC:100000:sawi

Result:
"txid"                   (string) The transaction id.

Examples:
```bash
 ./coind submitcdpstaketx "WiZx6rrsBn9sHjwpvdwtMNNX2o31s3DEHH" "WICC:20000000000:sawi" "WUSD:3000000:sawi" "b850d88bf1bed66d43552dd724c18f10355e9b6657baeae262b3c86a983bee71" "WICC:1000000:sawi"
```

As json rpc call
```bash
 curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitcdpstaketx", "params": ["WiZx6rrsBn9sHjwpvdwtMNNX2o31s3DEHH", "WICC:2000000000:sawi", "WUSD:3000000:sawi", "b850d88bf1bed66d43552dd724c18f10355e9b6657baeae262b3c86a983bee71", "WICC:1000000:sawi"
] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

<br>

---

### Close a CDP

* Function: `submitcdpredeemtx`
* Usage: `submitcdpredeemtx "addr" "cdp_id" repay_amount redeem_amount ["symbol:fee:unit"]`

submit a CDP Redemption Tx

Arguments:

1. "addr" :              (string, required) CDP redemptor's address
2. "cdp_id":             (string, required) ID of existing CDP (tx hash of the first CDP Stake Tx)
3. "repay_amount":       (numeric, required) scoins (E.g. WUSD) to repay into the CDP, boosted by 10^8
4. "redeem_amount":      (numeric, required) bcoins (E.g. WICC) to redeem from the CDP, boosted by 10^8
5. "symbol:fee:unit":    (string:numeric:string, optional) fee paid to miner, default is WICC:100000:sawi

Result:
"txid"                   (string) The transaction id.

Examples:
```bash
 ./coind submitcdpredeemtx "WiZx6rrsBn9sHjwpvdwtMNNX2o31s3DEHH" "b850d88bf1bed66d43552dd724c18f10355e9b6657baeae262b3c86a983bee71" 20000000000 40000000000 "WICC:1000000:sawi"
```

As json rpc call
```bash
 curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitcdpredeemtx", "params": ["WiZx6rrsBn9sHjwpvdwtMNNX2o31s3DEHH", "b850d88bf1bed66d43552dd724c18f10355e9b6657baeae262b3c86a983bee71", 20000000000, 40000000000, "WICC:1000000:sawi"
] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

<br>

---

### Check a CDP

* Function: `getusercdp "addr"`
* Usage: `getcdp "cdp_id"`

get account's cdp.

Arguments:

1."addr": (string, required) CDP owner's account addr

Result:

Examples:
```bash
./coind getusercdp "WiZx6rrsBn9sHjwpvdwtMNNX2o31s3DEHH"
``` 


As json rpc call
```bash
 curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "getusercdp", "params": ["WiZx6rrsBn9sHjwpvdwtMNNX2o31s3DEHH"
] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

<br>

---


### Liquidate a CDP

* Function: `submitcdpliquidatetx`
* Usage: `submitcdpliquidatetx "addr" "cdp_id" liquidate_amount [symbol:fee:unit]`

submit a CDP Liquidation Tx

Arguments:

1. "addr" :              (string, required) CDP liquidator's address
2. "cdp_id":             (string, required) ID of existing CDP (tx hash of the first CDP Stake Tx)
3. "liquidate_amount":   (numeric, required) WUSD coins to repay to CDP, boosted by 10^8 (penalty fees deducted separately from sender account)
4. "symbol:fee:unit":    (string:numeric:string, optional) fee paid to miner, default is WICC:100000:sawi

Result:
"txid" (string) The transaction id.

Examples:

```bash
 ./coind submitcdpliquidatetx "WiZx6rrsBn9sHjwpvdwtMNNX2o31s3DEHH"  "b850d88bf1bed66d43552dd724c18f10355e9b6657baeae262b3c86a983bee71" 20000000000 "WICC:1000000:sawi"
```

As json rpc call

```bash
 curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "submitcdpliquidatetx", "params": ["WiZx6rrsBn9sHjwpvdwtMNNX2o31s3DEHH", "b850d88bf1bed66d43552dd724c18f10355e9b6657baeae262b3c86a983bee71", 2000000000, "WICC:1000000:sawi"
] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

<br>

---
