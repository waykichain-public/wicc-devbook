# build WASM  
---

Smart Contracts Quick Start Guide - WASM  
--

## WASM compilation of environment configuration  
 
Guided Installation (Building from source code)   

```
$ git clone --recurse https://github.com/WaykiChain/wicc-wasm-cdt.git  
$ cd wicc-wasm-cdt  
$ ./build.sh
$ sudo ./install.sh
```
If there is no error, it's prove compilation succeeds  

----
