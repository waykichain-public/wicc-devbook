# WRC30

## Introduction
`WRC30` is the iToken standard of Digital Asset Which Base on Waykichain public chain.

## Character
1. Replace of coind Standard Which base on public chain bottom develop

2. The stability currency can be lent afterdecentralized governance

3. Support deal in WaykiDEX(https://dex.waykichain.com/)

4. More friendly wiith developer, and it's easier in development process of issue and transfer  

## Fee of issue asset  and rule introduce

1、Fee of issue asset : 550 WICC

2、Asset issuer and asset owner must be [regid  is already there](../../Summary/account/)

---

## WRC30 asset issue

### Issue WRC30 digital asset through [Waykichain](https://www.waykichain.com/WaykiTimes.html)

![](../images/wrc30.png)

---

### Issue WRC30 digital asset through WASM System contract

---
issue，mint, transfer All through **submittx** API operator，set contract as 0-800，it's means system contracts,detail need refer to [submittx in SmartContract-wasm](../../JsonRpcApi/contract_wasm/)
---

#### 1. use api `issue`  to issue WRC30 digital asset 
fee of issue asset : 550 WICC

transaction fee: refer to [Fee list](../../Summary/transaction/)

**Parameters**

`addr`           sender‘s address  

`symbol`    asset symbol, incloud 6~7[A-Z]characters（prefix fixed with 8 to indicate 8-bit precision）   

`owner`     asset owner address  

`name`      asset name  1~32 characters  

`total_supply`  Issuance of assets，max：900 x 10^8 x 10^8 unit`sawi`  

`mintable`      Whether the assets can be added `true`或 `false`  

`memo`  memo  

**Returns**  

`txid`  The transaction id.  

**Example**  
```
// Request
root@0e7612ac5bc5:/opt/wicc# coind submittx 0-2 0-800 issue '["8,HHHHHH", "0-2", "issue-test",10000000000,true,"issue uia"]'

// Respond
{
    "txid" : "bfbdcf4448ab5e440be11e4efd1b127472c6216ebddd31f281e4afe7a6ac6ddf",
    "tx_trace" : {
        "trx_id" : "bfbdcf4448ab5e440be11e4efd1b127472c6216ebddd31f281e4afe7a6ac6ddf",
        "elapsed" : 2542,
        "minimum_fee" : 100000,
        "traces" : [
            {
                "trx_id" : "bfbdcf4448ab5e440be11e4efd1b127472c6216ebddd31f281e4afe7a6ac6ddf",
                "receiver" : "0-800",
                "trx" : {
                    "contract" : "0-800",
                    "action" : "issue",
                    "authorization" : [
                        {
                            "account" : "0-2",
                            "permission" : "wasmio_owner"
                        }
                    ],
                    "data" : {
                        "symbol" : "8,HHHHHH",
                        "owner" : "0-2",
                        "name" : "issue-test",
                        "total_supply" : 10000000000,
                        "mintable" : true,
                        "memo" : "issue uia"
                    }
                }
            }
        ]
    }
}

// As json rpc call
       > curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method":"submittx", "params":["0-2", "0-800", "issue", '["8,HHHHHH", "0-2", "issue-test",10000000000,true,"issue to bob"]']}' -H 'Content-Type: application/json;' http://127.0.0.1:8332
}
```

#### 2. Verify that the asset was issued successfully
call the RPC interface [getaccountinfo](../../JsonRpcApi/account/#getaccountinfo) to check if tokens list of asset owner is adding in issueed

or call the RPC interface `getassetinfo`  to check the information of issue coind

**Parameters**

`asset_symbol`    asset symbol, incloud 6~7[A-Z]characters  

**Returns**

`asset_symbol` asset symbol   

`asset_name`  asset name  

`asset_type`  asset type 

`perms_sum`   permission  

`owner_regid`  asset owner regid  

`total_supply`  Issuance of assets，unit `sawi`  

`mintable`  Whether the assets can be added `true`或 `false` 

```
// Request
root@0e7612ac5bc5:/opt/wicc# coind getassetinfo HHHHHH

// Response
{
    "asset_symbol" : "HHHHHH",
    "asset_name" : "issue-test",
    "asset_type" : 3,
    "perms_sum" : "00011",
    "owner_regid" : "0-2",
    "total_supply" : 10000000000,
    "mintable" : true
}

// As json rpc call
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method": "getassetinfo", "params": [MINEUSD] }' -H 'Content-Type: application/json;' http://127.0.0.1:8332/
```

#### 3.using interface `transfer` going WRC30 asset transfer 

**Parameters**

`from`    send account

`to`       recive account 

`quantity`     transfer num  asset type  

`memo`  memo

**Returns**  

`txid` The transaction id.   

**Example**  
```
// Resquest
root@0e7612ac5bc5:/opt/wicc# coind submittx 0-2 0-800 transfer '["0-2", "0-3", "100.00000000 HHHHHH","transfer to bob"]'

// Respond
{
    "txid" : "23a22a9658173927e617ecfc30781ed1ad061beaddd355bcbf6e6c29b885c2f5",
    "tx_trace" : {
        "trx_id" : "23a22a9658173927e617ecfc30781ed1ad061beaddd355bcbf6e6c29b885c2f5",
        "elapsed" : 720,
        "minimum_fee" : 100000,
        "traces" : [
            {
                "trx_id" : "23a22a9658173927e617ecfc30781ed1ad061beaddd355bcbf6e6c29b885c2f5",
                "receiver" : "0-800",
                "trx" : {
                    "contract" : "0-800",
                    "action" : "transfer",
                    "authorization" : [
                        {
                            "account" : "0-2",
                            "permission" : "wasmio_owner"
                        }
                    ],
                    "data" : {
                        "from" : "0-2",
                        "to" : "0-3",
                        "quantity" : "100.00000000 HHHHHH",
                        "memo" : "transfer to bob"
                    }
                },
                "inline_traces" : [
                    {
                        "trx_id" : "23a22a9658173927e617ecfc30781ed1ad061beaddd355bcbf6e6c29b885c2f5",
                        "receiver" : "0-2",
                        "trx" : {
                            "contract" : "0-800",
                            "action" : "transfer",
                            "authorization" : [
                                {
                                    "account" : "0-2",
                                    "permission" : "wasmio_owner"
                                }
                            ],
                            "data" : {
                                "from" : "0-2",
                                "to" : "0-3",
                                "quantity" : "100.00000000 HHHHHH",
                                "memo" : "transfer to bob"
                            }
                        }
                    },
                    {
                        "trx_id" : "23a22a9658173927e617ecfc30781ed1ad061beaddd355bcbf6e6c29b885c2f5",
                        "receiver" : "0-3",
                        "trx" : {
                            "contract" : "0-800",
                            "action" : "transfer",
                            "authorization" : [
                                {
                                    "account" : "0-2",
                                    "permission" : "wasmio_owner"
                                }
                            ],
                            "data" : {
                                "from" : "0-2",
                                "to" : "0-3",
                                "quantity" : "100.00000000 HHHHHH",
                                "memo" : "transfer to bob"
                            }
                        }
                    }
                ]
            }
        ]
    }
}

// As json rpc call
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method":"submittx", "params":["0-2", "0-800", "transfer", '["0-2", "0-3", "100.00000000 HHHHHH","transfer to bob"]']}' -H 'Content-Type: application/json;' http://127.0.0.1:8332
```

#### 4.using interface of RPC [getaccountinfo](../../JsonRpcApi/account/#getaccountinfo) to check there is get WRC asset of recipient account 

```
// Request
root@0e7612ac5bc5:/opt/wicc# coind getaccountinfo 0-3

// Respond
{
    "address" : "wNuJM44FPC5NxearNLP98pg295VqP7hsqu",
    "keyid" : "23e8b4ac5e3dea621474cad9d9dc4323018856c9",
    "regid" : "0-3",
    "regid_mature" : true,
    "owner_pubkey" : "025a37cb6ec9f63bb17e562865e006f0bafa9afbd8a846bd87fc8ff9e35db1252e",
    "miner_pubkey" : "",
    "perms" : "1111111111",
    "tokens" : {
        "HHHHHH" : {
            "free_amount" : 10000000000,
            "staked_amount" : 0,
            "frozen_amount" : 0,
            "voted_amount" : 0,
            "pledged_amount" : 0,
            "total_amount" : 10000000000
        },
        "WICC" : {
            "free_amount" : 30201100011,
            "staked_amount" : 0,
            "frozen_amount" : 0,
            "voted_amount" : 0,
            "pledged_amount" : 0,
            "total_amount" : 30201100011
        }
    },
    "received_votes" : 0,
    "vote_list" : [
    ],
    "onchain" : true,
    "in_wallet" : true,
    "pubkey_registered" : true,
    "cdp_list" : [
    ]
}
```

#### 5、using interface `mint`  to mint，increase asset  

**Parameters**  

`to`      recive account  

`quantity`     transfer num, asset type

`memo`  memo  

**Returns**  

`txid` The transaction id.  

**Example**  

```
// Resquest
root@0e7612ac5bc5:/opt/wicc# coind submittx 0-2 0-800 mint '["0-3", "241.00000000 HHHHHH","mint"]'

// Respond
{
    "txid" : "4b10fca887fe3abd4d74eda6afa0088d1fee5cb037dc292744b8fba014ef8205",
    "tx_trace" : {
        "trx_id" : "4b10fca887fe3abd4d74eda6afa0088d1fee5cb037dc292744b8fba014ef8205",
        "elapsed" : 75,
        "minimum_fee" : 100000,
        "traces" : [
            {
                "trx_id" : "4b10fca887fe3abd4d74eda6afa0088d1fee5cb037dc292744b8fba014ef8205",
                "receiver" : "0-800",
                "trx" : {
                    "contract" : "0-800",
                    "action" : "mint",
                    "authorization" : [
                        {
                            "account" : "0-2",
                            "permission" : "wasmio_owner"
                        }
                    ],
                    "data" : {
                        "to" : "0-3",
                        "quantity" : "241.00000000 HHHHHH",
                        "memo" : "mint"
                    }
                },
                "inline_traces" : [
                    {
                        "trx_id" : "4b10fca887fe3abd4d74eda6afa0088d1fee5cb037dc292744b8fba014ef8205",
                        "receiver" : "0-3",
                        "trx" : {
                            "contract" : "0-800",
                            "action" : "mint",
                            "authorization" : [
                                {
                                    "account" : "0-2",
                                    "permission" : "wasmio_owner"
                                }
                            ],
                            "data" : {
                                "to" : "0-3",
                                "quantity" : "241.00000000 HHHHHH",
                                "memo" : "mint"
                            }
                        }
                    }
                ]
            }
        ]
    }
}

// As json rpc call
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method":"submittx", "params":["0-2", "0-800", "mint", '["0-3", "241.00000000 HHHHHH","mint to bob"]']}' -H 'Content-Type: application/json;' http://127.0.0.1:8332

```

#### 6、using interface `burn`  to burn asset  

**Parameters**  

`owner`       asset owner

`quantity`     burn num, asset type 

`memo`  memo  

**Returns**  

`txid` The transaction id.     

**Example**  

```
// Resquest
root@0e7612ac5bc5:/opt/wicc# coind submittx 0-2 0-800 burn '["0-2","1.00000000 HHHHHH","burn"]'
// Respond
{
    "txid" : "7d9ec072d21201508cb6c9786bd6f5fcea9abe6662f6dbcf55dbafbdedf7c41e",
    "tx_trace" : {
        "trx_id" : "7d9ec072d21201508cb6c9786bd6f5fcea9abe6662f6dbcf55dbafbdedf7c41e",
        "elapsed" : 56,
        "minimum_fee" : 100000,
        "traces" : [
            {
                "trx_id" : "7d9ec072d21201508cb6c9786bd6f5fcea9abe6662f6dbcf55dbafbdedf7c41e",
                "receiver" : "0-800",
                "trx" : {
                    "contract" : "0-800",
                    "action" : "burn",
                    "authorization" : [
                        {
                            "account" : "0-2",
                            "permission" : "wasmio_owner"
                        }
                    ],
                    "data" : {
                        "owner" : "0-2",
                        "quantity" : "1.00000000 HHHHHH",
                        "memo" : "burn"
                    }
                }
            }
        ]
    }
}

// As json rpc call
> curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method":"submittx", "params":["0-2", "0-800", "burn", '["0-2", "1.00000000 HHHHHH","burn to bob"]']}' -H 'Content-Type: application/json;' http://127.0.0.1:8332
```

---
