## WaykiChain Offline Wallet Tools SDK (WaykiChain Offline Wallet Utilities SDK)

* WaykiChain Chain Wallet Utils (WaykiChain Offline Wallet Utils)

* Development Language: Kotlin

**Core Functions**

* Key and address generation
* Offline Transaction Signing
 
<br>

---
## Pull code

```
// Download by CMD
git clone https://github.com:WaykiChain/wicc-wallet-utils-kotlin.git

// Download by https
https://github.com/WaykiChain/wicc-wallet-utils-kotlin
```

---
## Build
**suggest platform** : `IntelliJ IDEA`  
check local jdk version by use CMD
``` 
java -version
```
it needn't modify if the jdk version in local is 8, and if that is 14, then should set `distributionUrl` value as follows in directory `gradle`:`wrapper`:`gradle-wrapper.properties`
```
distributionUrl=https\://services.gradle.org/distributions/gradle-4.8.1-bin.zip
```

## Usage (Usage)

**Create Wallet (WaykiChain Create Wallet)**

Generate mnemonics and private keys to manage your wallet.

- [1. Generate mnemonic GenerateMnemonics. You will get 12 words](https://github.com/WaykiChain/wicc-wallet-utils-kotlin/blob/master/src/test/kotlin/com/waykichain/wallet/TestWallet.kt)

```kotlin
 // Resquest:
var words = MnemonicUtil.randomMnemonicCodes()
logger.info(words.toString())
  
//Respond
info: [vote, image, poverty, list, book, siren, dentist, cave, north, right, suit, liberty]
```

- [2. Generate wallet generate wallet from mnemonic](https://github.com/WaykiChain/wicc-wallet-utils-kotlin/blob/master/src/test/kotlin/com/waykichain/wallet/TestWallet.kt)

```kotlin
 // Request
val words = "vote, image, poverty, list, book, siren, dentist, cave, north, right, suit, liberty"
val networkParameters = WaykiTestNetParams.instance //generate Testnet Address From Mnemonic
//val networkParameters = WaykiMainNetParams.instance //generate Mainnet Address From Mnemonic
val wallet= BIP44Util.generateWaykiWallet(words, networkParameters)
logger.info("\nmnemonic: $words\naddress:   ${wallet.address} \n Private key: ${wallet.privateKey} \nPublic Key: ${wallet.pubKey}")

// Respond
info: 
mnemonic: vote, image, poverty, list, book, siren, dentist, cave, north, right, suit, liberty
address:   waMDJew9pm5e6fT7aCaWG1yBwcPx1mW7B2 
Private key: Y9wDyMys64KVhqwAVxbAB4aYDNVQ4HpRhQ7FLWFC3MhNNXz4JHot 
Public Key: 02a89bd7e93f119c37a02c3397ca153c22c9631677acd1fa9cd77ea5c20f4e8f6d
```

- [3. Import private key Import private key](https://github.com/WaykiChain/wicc-wallet-utils-kotlin/blob/master/src/test/kotlin/com/waykichain/wallet/TestWallet.kt)

```kotlin
// Resquest
val params = WaykiTestNetParams.instance //TestNet
val privKeyWiF = “Y9wDyMys64KVhqwAVxbAB4aYDNVQ4HpRhQ7FLWFC3MhNNXz4JHot”
val key = DumpedPrivateKey.fromBase58(params, privKeyWiF).key
logger.info(Utils.HEX.encode(key.pubKeyHash))

// Respond
info: 94ed078235cdfdb52e35c2fdf4cee77e286361fa
```

<br>

### Transaction Signature (WaykiChain Sign Transaction)

By signing the transaction with a private key, you can submit an offline signature rawtx transaction via the Bass service.

Signing a transaction with a private key, you can submit your offline signature rawtx transaction by bass service.

| BassNetwork | ApiAddr |
|-------------- |---------------------------------- |
TestNetwork | https://baas-test.wiccdev.org/v2/api/swagger-ui.html#!/ |
| ProdNetwork | https://baas.wiccdev.org/v2/api/swagger-ui.html#!/ | |

**Submit transaction Submit raw string:**

Mainnet <https://baas.wiccdev.org/v2/api/swagger-ui.html#!/transaction-controller/offlinTransactionUsingPOST> 

TestNet <https://baas-test.wiccdev.org/v2/api/swagger-ui.html#!/transaction-controller/offlinTransactionUsingPOST>

**Get the block height Get block height:**

MainNet<https://baas.wiccdev.org/v2/api/swagger-ui.html#!/block-controller/getBlockCountUsingPOST>

TestNet <https://baas-test.wiccdev.org/v2/api/swagger-ui.html#!/block-controller/getBlockCountUsingPOST>

<br>


#### WaykiChain Transaction

   [**WICC Transaction Unit description**](https://wicc-devbook.readthedocs.io/zh_CN/latest/Problem/question/)

- [1. Sign Register Account Transaction](https://githuCb.com/WaykiChain/wicc-wallet-utils-kotlin/blob/master/src/test/kotlin/com/waykichain/wallet/TestTransaction.kt)

**NOTE**

Wallet registration transactions are not required, you can activate your wallet in other transactions via public key parameters.
(The register transaction is not required, you can activate wallet by public key in other transactions)

```kotlin
// Resquest
val wallet = LegacyWallet()
val netParams = WaykiTestNetParams.instance
val privKeyWiF = "Y9XMqNzseQFSK32SvMDNF9J7xz1CQmHRsmY1hMYiqZyTck8pYae3"
val key = DumpedPrivateKey.fromBase58(netParams, privKeyWiF).key
val txParams = WaykiRegisterAccountTxParams(key.publicKeyAsHex, null, 429821, 10000, CoinType.WICC.type)
txParams.signTx(key)
val tx = wallet.createRegisterTransactionRaw(txParams)
logger.info("${tx.length} - $tx")

// Respond
信息: 230 - 0201999c7d2102a722a3a94fb41d92bcf9d54cd76ea40c8b0c223d6f0570389b775120c5e487640083e1ac0046304402205304902f6ae8470e7c294b8abe7fdd5a9847d8980914234c9ddb9b6098e473d002200ad2d0238292285394447905cb20b7275cd2daf3a68d1237a1200982b99172bc
```

- [2. Sign Common Transaction](https://github.com/WaykiChain/wicc-wallet-utils-kotlin/blob/master/src/test/kotlin/com/waykichain/wallet/TestTransaction.kt)

```kotlin
        Val wallet = LegacyWallet()
        Val netParams = WaykiTestNetParams.instance
        Val srcPrivKeyWiF = "Y6J4aK6Wcs4A3Ex4HXdfjJ6ZsHpNZfjaS4B9w7xqEnmFEYMqQd13"
        Val srcKey = DumpedPrivateKey.fromBase58(netParams, srcPrivKeyWiF).key
        Val pubKey = srcKey.publicKeyAsHex //user public key
        Val destAddr = "wWTStcDL4gma6kPziyHhFGAP6xUzKpA5if"
        Val memo="test transfer"
        Val txParams = WaykiCommonTxParams(WaykiNetworkType.TEST_NET, 34550, pubKey,10000,1100000000000, "0-1", destAddr,memo)
        txParams.signTx(srcKey)
        Val tx = wallet.createCommonTransactionRaw(txParams)
```

`WaykiCommonTxParams` prase function

```
WaykiCommonTxParams(networkType: WaykiNetworkType, nValidHeight: Long,pubKey:String, fees: Long, val value: Long, val srcRegId: String, destAddr: String,val memo: String)
```

|param|func|
|--------|-------|
|networkType|connect type : <br> TestNet:WaykiNetworkType.TEST_NET<br> MainNet：WaykiNetworkType.MAIN_NET|
|nValidHeight|valid height|
|pubKey|public key|
|fees|charge with miner|
|value|transfer charge|
|srcRegId|sender regid|
|destAddr|receive address|
|memo| |


- [3. Multi-Currency Transfer Transaction](https://github.com/WaykiChain/wicc-wallet-utils-kotlin/blob/master/src/test/kotlin/com/waykichain/wallet/TestTransaction.kt)

```kotlin
         //CoinSymbol:Supported (WICC WUSD WGRT)
         //FeeSymbol:Supported (WICC WUSD WGRT)
        Val wallet = LegacyWallet()
        Val netParams = WaykiTestNetParams.instance
        Val srcPrivKeyWiF = "Y6J4aK6Wcs4A3Ex4HXdfjJ6ZsHpNZfjaS4B9w7xqEnmFEYMqQd13"
        Val srcKey = DumpedPrivateKey.fromBase58(netParams, srcPrivKeyWiF).key
        Val pubKey = srcKey.publicKeyAsHex //user publickey hex string
        Val nValidHeight = 440601L
        Val coinSymbol = CoinType.WICC.type //coin symbol
        Val coinAmount = 100000000L //transfer amount
        Val feeSymbol = CoinType.WICC.type
        Val fees = 100000L //Minimum fee 10000sawi
        Val regid = "0-1" // User regid
        Val destAddr = "wWXYkAhNdNdv5LBEavQB1aUJeYqApNc2YW"
        Val memo = "test transfer" //transfer memo
        Val txParams = WaykiUCoinTxParams(WaykiNetworkType.TEST_NET, nValidHeight, regid, pubKey, destAddr, coinSymbol, coinAmount, feeSymbol, fees, memo)
        txParams.signTx(srcKey)
        Val tx = wallet.createUCoinTransactionRaw(txParams)
```

- [4. Sign Delegate Transaction](https://github.com/WaykiChain/wicc-wallet-utils-kotlin/blob/master/src/test/kotlin/com/waykichain/wallet/TestTransaction.kt)

```kotlin
        Val wallet = LegacyWallet()
        Val netParams = WaykiTestNetParams.instance
        Val srcPrivKeyWiF = "Y6CFeJthSWMPRRcEu734u4ovBfjRp3ytngt9iGEfsMvqxPmKo2Vy"
        Val srcKey = DumpedPrivateKey.fromBase58(netParams, srcPrivKeyWiF).key
        Val srcAddress = LegacyAddress.fromPubKeyHash(netParams, srcKey.pubKeyHash).toString()
        Val votedPubKey=Utils.HEX.decode("2ba8329bc5507c867bdc9be0ce487419de3c6737ae6754657db62f2df02ff07f")//public key as hex string
        //VoteOperType.ADD_FUND vote
        //VoteOperType.MINUS_FUND //Cancel the vote
        Val array1 = OperVoteFund(VoteOperType.ADD_FUND.value, votedPubKey, 200000000)
        Val array2 = arrayOf(array1)
        Val txParams = WaykiDelegateTxParams("25813-1",srcKey.publicKeyAsHex, array2, 10000000, 479796)
        txParams.signTx(srcKey)
        Val tx = wallet.createDelegateTransactionRaw(txParams)
```

- [5. Sign Invoke Contract Transaction](https://github.com/WaykiChain/wicc-wallet-utils-kotlin/blob/master/src/test/kotlin/com/waykichain/wallet/TestTransaction.kt)

[**WRC20 Asset Invoke**](https://www.wiccdev.org/book/en/Contract/ico_sample.html#%E5%AF%B9%E4%BB%A3%E5%B8%81%E8%BF%9B%E8%A1%8C%E8%BD%AC%E8%B4%A6)

````kotlin
        //Activate WRC20 Assets
        / / Activate WRC20 assets
        Val wallet = LegacyWallet()
        Val netParams = WaykiMainNetParams.instance
        Val srcPrivKeyWiF = "Y6J4aK6Wcs4A3Ex4HXdfjJ6ZsHpNZfjaS4B9w7xqEnmFEYMqQd13"
        Val srcKey = DumpedPrivateKey.fromBase58(netParams, srcPrivKeyWiF).key
        Val regId = "926152-1"
        Val appId = "128711-1"
        Val contract = "f0110000"
        Val contractByte = ContractUtil.hexString2binaryString(contract)
        Val txParams = WaykiContractTxParams(srcKey.publicKeyAsHex, 494454, 100000, 0, regId, appid, contractByte, CoinType.WICC.type)
        txParams.signTx(srcKey)
        Val tx = wallet.createContractTransactionRaw(txParams)
````
   
````kotlin
        //WRC20 Transfer
        //WRC20 transfer
        Val wallet = LegacyWallet()
        Val netParams = WaykiTestNetParams.instance
        Val srcPrivKeyWiF = "Y6J4aK6Wcs4A3Ex4HXdfjJ6ZsHpNZfjaS4B9w7xqEnmFEYMqQd13"
        Val srcKey = DumpedPrivateKey.fromBase58(netParams, srcPrivKeyWiF).key
        Val regId = "926152-1"
        Val appId = "128711-1" //Contract Regid
        Val wrc20Amount = 10000L // transfer 10000 WRC Token
        Val destAddress="wNPWqv9bvFCnMm1ddiQdH7fUwUk2Qgrs2N"
        Val contractByte = ContractUtil.transferWRC20Contract(wrc20Amount,destAddress)
        Val txParams = WaykiContractTxParams(srcKey.publicKeyAsHex, 494454, 100000, 0, regId, appId, contractByte, CoinType.WICC.type)
        txParams.signTx(srcKey)
        Val tx = wallet.createContractTransactionRaw(txParams)
````

<br>

#### CDP Transaction

Any user holding WICC can send WICC to CDP (mortgage debt location) to obtain a certain percentage of WUSD. A user can only have one CDP unless the previous CDP has been destroyed.

Any user holding a WICC can send a WICC to the CDP (Collaterized Debt Position) to obtain a certain percentage of WUSD.a user can only have one cdp unless the previous cdp has been destroyed.

- [CDP Ptp Stake Transaction](https://github.com/WaykiChain/wicc-wallet-utils-kotlin/blob/master/src/test/kotlin/com/waykichain/wallet/TestTransaction.kt)

````kotlin
        Val nValidHeight = 283308L
        Val fee = 100000L
        Val userId = "0-1" //wallet regid
        Val cdpTxid = "009c0e665acdd9e8ae754f9a51337b85bb8996980a93d6175b61edccd3cdc144" //wallet cdp create tx hash
        Val feeSymbol = CoinType.WICC.type //fee symbol
        Val bCoinSymbol = CoinType.WICC.type //stake coin symbol
        Val sCoinSymbol = CoinType.WUSD.type // get coind symbol
        Val bCoinToStake = 100000000L //stake amount
        Val sCoinToMint = 50000000L //get amount

        Val wallet = LegacyWallet()
        Val netParams = WaykiTestNetParams.instance
        Val srcPrivKeyWiF = "Y6J4aK6Wcs4A3Ex4HXdfjJ6ZsHpNZfjaS4B9w7xqEnmFEYMqQd13"
        Val srcKey = DumpedPrivateKey.fromBase58(netParams, srcPrivKeyWiF).key
        //if no wallet regid , you can use wallet public key
        Val userPubKey = srcKey.publicKeyAsHex //wallet publickey hex string
        Val txParams = WaykiCdpStakeTxParams(nValidHeight, fee, userId, userPubKey, cdpTxid, feeSymbol, bCoinSymbol, sCoinSymbol, bCoinToStake, sCoinToMint)
        txParams.signTx(srcKey)
        Val tx = wallet.createCdpStakeTransactionRaw(txParams)
````


- [CDP Redeem Transaction](https://github.com/WaykiChain/wicc-wallet-utils-kotlin/blob/master/src/test/kotlin/com/waykichain/wallet/TestTransaction.kt)

```kotlin
        Val nValidHeight = 283308L
        Val fee = 100000L
        Val userId = "0-1" //wallet regid
        Val cdpTxid = "009c0e665acdd9e8ae754f9a51337b85bb8996980a93d6175b61edccd3cdc144" //wallet cdp create tx hash
        Val feeSymbol = CoinType.WICC.type //fee symbol
        Val sCoinsToRepay = 50000000L //repay amount
        Val bCoinsToRedeem = 100000000L //redeem amount

        Val wallet = LegacyWallet()
        Val netParams = WaykiTestNetParams.instance
        Val srcPrivKeyWiF = "Y6J4aK6Wcs4A3Ex4HXdfjJ6ZsHpNZfjaS4B9w7xqEnmFEYMqQd13"
        Val srcKey = DumpedPrivateKey.fromBase58(netParams, srcPrivKeyWiF).key
        //if no wallet regid , you can use wallet public key
        Val userPubKey = srcKey.publicKeyAsHex //wallet publickey hex string
        Val txParams = WaykiCdpRedeemTxParams(nValidHeight, fee, userId, userPubKey, cdpTxid, feeSymbol, sCoinsToRepay, bCoinsToRedeem)
        txParams.signTx(srcKey)
        Val tx = wallet.createCdpRedeemTransactionRaw(txParams)
```

- [CDP Liquidate Transaction](https://github.com/WaykiChain/wicc-wallet-utils-kotlin/blob/master/src/test/kotlin/com/waykichain/wallet/TestTransaction.kt )

```kotlin
        Val nValidHeight = 283308L
        Val fee = 100000L
        Val userId = "0-1" //wallet regid
        Val cdpTxid = "009c0e665acdd9e8ae754f9a51337b85bb8996980a93d6175b61edccd3cdc144" //wallet cdp create tx hash
        Val feeSymbol = CoinType.WICC.type //fee symbol
        Val sCoinsToLiquidate = 10000000L //Liquidate amount

        Val wallet = LegacyWallet()
        Val netParams = WaykiTestNetParams.instance
        Val srcPrivKeyWiF = "Y6J4aK6Wcs4A3Ex4HXdfjJ6ZsHpNZfjaS4B9w7xqEnmFEYMqQd13"
        Val srcKey = DumpedPrivateKey.fromBase58(netParams, srcPrivKeyWiF).key
        //if no wallet regid , you can use wallet public key
        Val userPubKey = srcKey.publicKeyAsHex //wallet publickey hex string
        Val txParams = WaykiCdpLiquidateTxParams(nValidHeight, fee, userId, userPubKey, cdpTxid, feeSymbol, sCoinsToLiquidate)
        txParams.signTx(srcKey)
        Val tx = wallet.createCdpLiquidateTransactionRaw(txParams)
```

<br>

#### DEX Transaction

WaykiChain Chain decentralized exchange (WaykiChain decentralized exchange).
- [Sign Dex Sell Limit Transaction](https://github.com/WaykiChain/wicc-wallet-utils-kotlin/blob/master/src/test/kotlin/com/waykichain/wallet/TestTransaction.kt)

```kotlin
        Val nValidHeight = 283308L
        Val fee = 100000L
        Val userId = "0-1" //wallet regid
        Val feeSymbol = CoinType.WICC.type //fee symbol
        Val wallet = LegacyWallet()
        Val netParams = WaykiTestNetParams.instance
        Val srcPrivKeyWiF = "Y6J4aK6Wcs4A3Ex4HXdfjJ6ZsHpNZfjaS4B9w7xqEnmFEYMqQd13"
        Val txType = WaykiTxType.DEX_SELL_LIMIT_ORDER_TX // Limit Sell Order
        Val srcKey = DumpedPrivateKey.fromBase58(netParams, srcPrivKeyWiF).key
        //if no wallet regid , you can use wallet public key
        Val userPubKey = srcKey.publicKeyAsHex //wallet publickey hex string
        Val coinSymbol = CoinType.WUSD.type
        Val assetSymbol = CoinType.WICC.type
        Val assetAmount = 100 * 100000000L
        Val askPrice = 1 * 10000L
        Val txParams = WaykiDexLimitTxParams(nValidHeight, fee, userId, userPubKey,
                feeSymbol, coinSymbol, assetSymbol, assetAmount, askPrice, txType)
        txParams.signTx(srcKey)
        Val tx = wallet.createDexLimitTransactionRaw(txParams)
```

- [Sign Dex Buy Limit Transaction](https://github.com/WaykiChain/wicc-wallet-utils-kotlin/blob/master/src/test/kotlin/com/waykichain/wallet/TestTransaction.kt)

```kotlin
        Val nValidHeight = 283308L
        Val fee = 100000L
        Val userId = "0-1" //wallet regid
        Val feeSymbol = CoinType.WICC.type //fee symbol
        Val wallet = LegacyWallet()
        Val netParams = WaykiTestNetParams.instance
        Val srcPrivKeyWiF = "Y6J4aK6Wcs4A3Ex4HXdfjJ6ZsHpNZfjaS4B9w7xqEnmFEYMqQd13"
        Val txType = WaykiTxType.DEX_BUY_LIMIT_ORDER_TX // Limit buy order
        Val srcKey = DumpedPrivateKey.fromBase58(netParams, srcPrivKeyWiF).key
        //if no wallet regid , you can use wallet public key
        Val userPubKey = srcKey.publicKeyAsHex //wallet publickey hex string
        Val coinSymbol = CoinType.WUSD.type
        Val assetSymbol = CoinType.WICC.type
        Val assetAmount = 100 * 100000000L
        Val bidPrice = 10 * 10000L
        Val txParams = WaykiDexLimitTxParams(nValidHeight, fee, userId, userPubKey,
                feeSymbol, coinSymbol, assetSymbol, assetAmount, bidPrice, txType)
        txParams.signTx(srcKey)
        Val tx = wallet.createDexLimitTransactionRaw(txParams)
```

- [Sign Dex Market Sell Transaction](https://github.com/WaykiChain/wicc-wallet-utils-kotlin/blob/master/src/test/kotlin/com/waykichain/wallet/TestTransaction.kt)

```kotlin
        Val nValidHeight = 283308L
        Val fee = 100000L
        Val userId = "0-1" //wallet regid
        Val feeSymbol = CoinType.WICC.type //fee symbol
        Val wallet = LegacyWallet()
        Val netParams = WaykiTestNetParams.instance
        Val srcPrivKeyWiF = "Y6J4aK6Wcs4A3Ex4HXdfjJ6ZsHpNZfjaS4B9w7xqEnmFEYMqQd13"
        Val txType = WaykiTxType.DEX_SELL_MARKET_ORDER_TX //Market Sell Order
        Val srcKey = DumpedPrivateKey.fromBase58(netParams, srcPrivKeyWiF).key
        //if no wallet regid , you can use wallet public key
        Val userPubKey = srcKey.publicKeyAsHex //wallet publickey hex string
        Val coinSymbol = CoinType.WUSD.type
        Val assetSymbol = CoinType.WICC.type
        Val assetAmount = 100 * 100000000L
        Val txParams = WaykiDexMarketTxParams(nValidHeight, fee, userId, userPubKey,
                feeSymbol, coinSymbol, assetSymbol, assetAmount, txType)
        txParams.signTx(srcKey)
        Val tx = wallet.createDexMarketTransactionRaw(txParams)
```

- [Sign Dex Market Buy Transaction](https://github.com/WaykiChain/wicc-wallet-utils-kotlin/blob/master/src/test/kotlin/com/waykichain/wallet/TestTransaction.kt)

```kotlin
        Val nValidHeight = 283308L
        Val fee = 100000L
        Val userId = "0-1" //wallet regid
        Val feeSymbol = CoinType.WICC.type //fee symbol
        Val wallet = LegacyWallet()
        Val netParams = WaykiTestNetParams.instance
        Val srcPrivKeyWiF = "Y6J4aK6Wcs4A3Ex4HXdfjJ6ZsHpNZfjaS4B9w7xqEnmFEYMqQd13"
        Val txType = WaykiTxType.DEX_SELL_MARKET_ORDER_TX //Market price to pay
        Val srcKey = DumpedPrivateKey.fromBase58(netParams, srcPrivKeyWiF).key
        //if no wallet regid , you can use wallet public key
        Val userPubKey = srcKey.publicKeyAsHex //wallet publickey hex string
        Val coinSymbol = CoinType.WUSD.type
        Val assetSymbol = CoinType.WICC.type
        Val assetAmount = 100 * 100000000L
        Val txParams = WaykiDexMarketTxParams(nValidHeight, fee, userId, userPubKey,
                feeSymbol, coinSymbol, assetSymbol, assetAmount, txType)
        txParams.signTx(srcKey)
        Val tx = wallet.createDexMarketTransactionRaw(txParams)
```

- [Sign Dex Cancel Transaction](https://github.com/WaykiChain/wicc-wallet-utils-kotlin/blob/master/src/test/kotlin/com/waykichain/wallet/TestTransaction.kt)

```kotlin
        Val nValidHeight = 283308L
        Val fee = 100000L
        Val userId = "0-1" //wallet regid
        Val feeSymbol = CoinType.WICC.type //fee symbol
        Val wallet = LegacyWallet()
        Val netParams = WaykiTestNetParams.instance
        Val srcPrivKeyWiF = "Y9XMqNzseQFSK32SvMDNF9J7xz1CQmHRsmY1hMYiqZyTck8pYae3"
        Val srcKey = DumpedPrivateKey.fromBase58(netParams, srcPrivKeyWiF).key
        //if no wallet regid , you can use wallet public key
        Val userPubKey = srcKey.publicKeyAsHex //wallet publickey hex string
        Println(userPubKey)
        Val dexOrderId="009c0e665acdd9e8ae754f9a51337b85bb8996980a93d6175b61edccd3cdc144" //dex order tx hash
        Val txParams = WaykiDexCancelOrderTxParams(nValidHeight, fee, userId, userPubKey,
                feeSymbol,dexOrderId)
        txParams.signTx(srcKey)
        Val tx = wallet.createDexCancelOrderTransactionRaw(txParams)
```

<br>

## How to compile and package (How to build)

* Execution Command

```
Gradle jar -PwiccBuildJar
```

* Output:

```build/libs/wicc-wallet-utils-2.0.0.jar```

## Reference Projects
* https://bitcoinj.github.io/
* https://github.com/bitcoin/secp256k1

<br>

---
