# Golang Library
#### Golang Offline Signature Library Development Guide

https://github.com/WaykiChain/wicc-wallet-utils-go

# Java (Kotlin) Library
#### Java Offline Signature Library Development Guide

https://github.com/WaykiChain/wicc-wallet-utils-kotlin
