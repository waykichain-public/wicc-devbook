### WaykiChain developer documentation 

<br>

---

* **English version is the ```master``` branch**
* **Chinese version is the ```zh``` branch**

If you want to contribute and translate the developer documentation in your language you can follow these steps. 

We are using [mkdocs](https://www.mkdocs.org/) to develop our documentation.

We use [read the docs](readthedocs.org) to host our documentation.

<br>

# **How to Add or Update documentation:**
<br> 

## 1. `git clone git@gitlab.com:waykichain-public/wicc-devbook.git`
## 2. `git checkout zh`   
`git checkout zh` updates the Chinese documentation

`git checkout kr` updates the Korean docs
 
## 3. `pip install mkdocs` 
Downloads and installs mkdocs for local development.
## 4. `mkdocs serve`
Starts the mkdocs server to render your docs locally.
Allows you to update your docs in real time. So every change you make a change it will reload the page so you can see the result. 
## 5. `git push --set-upstream origin zh` 
`git push --set-upstream origin zh`  updates the chinese branch. 

`git push --set-upstream origin kr` updates the korean branch. etc...


































